﻿namespace Monolith_Launcher_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            BunifuAnimatorNS.Animation animation2 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation3 = new BunifuAnimatorNS.Animation();
            this.RoundEdge_Main = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.DragControl_Main = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.ToolbarMain = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.minibttn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.maxbttn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.xbttn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.InfoLabel = new System.Windows.Forms.Label();
            this.GModPane = new System.Windows.Forms.Panel();
            this.mrp_infotab = new System.Windows.Forms.Panel();
            this.axWebBrowser1 = new System.Windows.Forms.WebBrowser();
            this.nc1 = new Bunifu.Framework.UI.BunifuCards();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.nb1 = new System.Windows.Forms.RichTextBox();
            this.nt1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.SocialP = new System.Windows.Forms.Panel();
            this.linkLabel5 = new System.Windows.Forms.LinkLabel();
            this.bunifuFlatButton28 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton33 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.CommunityP = new System.Windows.Forms.Panel();
            this.bunifuFlatButton26 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.linkLabel4 = new System.Windows.Forms.LinkLabel();
            this.bunifuFlatButton29 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton30 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton31 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.MRPP = new System.Windows.Forms.Panel();
            this.bunifuFlatButton23 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton24 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton25 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.bunifuFlatButton6 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton19 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton22 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.NewsNInfo = new System.Windows.Forms.Panel();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.bunifuFlatButton7 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton9 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton8 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.playbttn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton5 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.SettingsPane = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.BetaCard1 = new Bunifu.Framework.UI.BunifuCards();
            this.label8 = new System.Windows.Forms.Label();
            this.fsbs = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.bunifuSeparator3 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label6 = new System.Windows.Forms.Label();
            this.ExtensionCard1 = new Bunifu.Framework.UI.BunifuCards();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.bunifuSeparator2 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label7 = new System.Windows.Forms.Label();
            this.UpdateCard1 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuFlatButton1 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuSeparator1 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label4 = new System.Windows.Forms.Label();
            this.IMGAni = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.SideMenu = new System.Windows.Forms.Panel();
            this.bunifuCircleProgressbar1 = new Bunifu.Framework.UI.BunifuCircleProgressbar();
            this.label11 = new System.Windows.Forms.Label();
            this.MonoInfoBttn = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.MenuAni = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.processchk = new System.Windows.Forms.Timer(this.components);
            this.AlertAni = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.crashchk = new System.Windows.Forms.Timer(this.components);
            this.bunifuFlatButton10 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton11 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton12 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.label1 = new System.Windows.Forms.Label();
            this.bunifuCards1 = new Bunifu.Framework.UI.BunifuCards();
            this.label2 = new System.Windows.Forms.Label();
            this.bunifuSeparator4 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label3 = new System.Windows.Forms.Label();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.bunifuCards2 = new Bunifu.Framework.UI.BunifuCards();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.bunifuSeparator6 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.bunifuiOSSwitch1 = new Bunifu.Framework.UI.BunifuiOSSwitch();
            this.label16 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.bunifuCards3 = new Bunifu.Framework.UI.BunifuCards();
            this.bunifuSeparator5 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label19 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.bunifuCards4 = new Bunifu.Framework.UI.BunifuCards();
            this.label20 = new System.Windows.Forms.Label();
            this.bunifuSeparator7 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label21 = new System.Windows.Forms.Label();
            this.bunifuFlatButton3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuCards5 = new Bunifu.Framework.UI.BunifuCards();
            this.label22 = new System.Windows.Forms.Label();
            this.bunifuSeparator8 = new Bunifu.Framework.UI.BunifuSeparator();
            this.label23 = new System.Windows.Forms.Label();
            this.bunifuFlatButton4 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.ToolbarMain.SuspendLayout();
            this.GModPane.SuspendLayout();
            this.mrp_infotab.SuspendLayout();
            this.nc1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.SocialP.SuspendLayout();
            this.CommunityP.SuspendLayout();
            this.MRPP.SuspendLayout();
            this.NewsNInfo.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SettingsPane.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.BetaCard1.SuspendLayout();
            this.ExtensionCard1.SuspendLayout();
            this.UpdateCard1.SuspendLayout();
            this.SideMenu.SuspendLayout();
            this.bunifuCards1.SuspendLayout();
            this.bunifuCards2.SuspendLayout();
            this.bunifuCards3.SuspendLayout();
            this.bunifuCards4.SuspendLayout();
            this.bunifuCards5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // RoundEdge_Main
            // 
            this.RoundEdge_Main.ElipseRadius = 9;
            this.RoundEdge_Main.TargetControl = this;
            // 
            // DragControl_Main
            // 
            this.DragControl_Main.Fixed = true;
            this.DragControl_Main.Horizontal = true;
            this.DragControl_Main.TargetControl = this.ToolbarMain;
            this.DragControl_Main.Vertical = true;
            // 
            // ToolbarMain
            // 
            this.ToolbarMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.ToolbarMain.Controls.Add(this.label13);
            this.ToolbarMain.Controls.Add(this.minibttn);
            this.ToolbarMain.Controls.Add(this.maxbttn);
            this.ToolbarMain.Controls.Add(this.xbttn);
            this.AlertAni.SetDecoration(this.ToolbarMain, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.ToolbarMain, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.ToolbarMain, BunifuAnimatorNS.DecorationType.None);
            this.ToolbarMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.ToolbarMain.Location = new System.Drawing.Point(0, 0);
            this.ToolbarMain.Name = "ToolbarMain";
            this.ToolbarMain.Size = new System.Drawing.Size(1308, 28);
            this.ToolbarMain.TabIndex = 0;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.IMGAni.SetDecoration(this.label13, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label13, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label13, BunifuAnimatorNS.DecorationType.None);
            this.label13.Font = new System.Drawing.Font("Franklin Gothic Medium", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.DimGray;
            this.label13.Location = new System.Drawing.Point(7, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(180, 21);
            this.label13.TabIndex = 13;
            this.label13.Text = "MONOLITH LAUNCHER";
            // 
            // minibttn
            // 
            this.minibttn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.minibttn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.minibttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.minibttn.BorderRadius = 0;
            this.minibttn.ButtonText = "Mini";
            this.minibttn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.minibttn, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.minibttn, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.minibttn, BunifuAnimatorNS.DecorationType.None);
            this.minibttn.DisabledColor = System.Drawing.Color.Gray;
            this.minibttn.Dock = System.Windows.Forms.DockStyle.Right;
            this.minibttn.Iconcolor = System.Drawing.Color.Transparent;
            this.minibttn.Iconimage = null;
            this.minibttn.Iconimage_right = null;
            this.minibttn.Iconimage_right_Selected = null;
            this.minibttn.Iconimage_Selected = null;
            this.minibttn.IconMarginLeft = 0;
            this.minibttn.IconMarginRight = 0;
            this.minibttn.IconRightVisible = true;
            this.minibttn.IconRightZoom = 0D;
            this.minibttn.IconVisible = true;
            this.minibttn.IconZoom = 90D;
            this.minibttn.IsTab = true;
            this.minibttn.Location = new System.Drawing.Point(1170, 0);
            this.minibttn.Name = "minibttn";
            this.minibttn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.minibttn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(56)))), ((int)(((byte)(61)))));
            this.minibttn.OnHoverTextColor = System.Drawing.Color.White;
            this.minibttn.selected = true;
            this.minibttn.Size = new System.Drawing.Size(46, 28);
            this.minibttn.TabIndex = 11;
            this.minibttn.Text = "Mini";
            this.minibttn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.minibttn.Textcolor = System.Drawing.Color.White;
            this.minibttn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.minibttn.Click += new System.EventHandler(this.BunifuFlatButton6_Click);
            // 
            // maxbttn
            // 
            this.maxbttn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.maxbttn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.maxbttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.maxbttn.BorderRadius = 0;
            this.maxbttn.ButtonText = "Max";
            this.maxbttn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.maxbttn, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.maxbttn, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.maxbttn, BunifuAnimatorNS.DecorationType.None);
            this.maxbttn.DisabledColor = System.Drawing.Color.Gray;
            this.maxbttn.Dock = System.Windows.Forms.DockStyle.Right;
            this.maxbttn.Iconcolor = System.Drawing.Color.Transparent;
            this.maxbttn.Iconimage = null;
            this.maxbttn.Iconimage_right = null;
            this.maxbttn.Iconimage_right_Selected = null;
            this.maxbttn.Iconimage_Selected = null;
            this.maxbttn.IconMarginLeft = 0;
            this.maxbttn.IconMarginRight = 0;
            this.maxbttn.IconRightVisible = true;
            this.maxbttn.IconRightZoom = 0D;
            this.maxbttn.IconVisible = true;
            this.maxbttn.IconZoom = 90D;
            this.maxbttn.IsTab = true;
            this.maxbttn.Location = new System.Drawing.Point(1216, 0);
            this.maxbttn.Name = "maxbttn";
            this.maxbttn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.maxbttn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(56)))), ((int)(((byte)(61)))));
            this.maxbttn.OnHoverTextColor = System.Drawing.Color.White;
            this.maxbttn.selected = true;
            this.maxbttn.Size = new System.Drawing.Size(46, 28);
            this.maxbttn.TabIndex = 12;
            this.maxbttn.Text = "Max";
            this.maxbttn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.maxbttn.Textcolor = System.Drawing.Color.White;
            this.maxbttn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.maxbttn.Click += new System.EventHandler(this.BunifuFlatButton3_Click_1);
            // 
            // xbttn
            // 
            this.xbttn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.xbttn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.xbttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.xbttn.BorderRadius = 0;
            this.xbttn.ButtonText = "Exit";
            this.xbttn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.xbttn, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.xbttn, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.xbttn, BunifuAnimatorNS.DecorationType.None);
            this.xbttn.DisabledColor = System.Drawing.Color.Gray;
            this.xbttn.Dock = System.Windows.Forms.DockStyle.Right;
            this.xbttn.Iconcolor = System.Drawing.Color.Transparent;
            this.xbttn.Iconimage = null;
            this.xbttn.Iconimage_right = null;
            this.xbttn.Iconimage_right_Selected = null;
            this.xbttn.Iconimage_Selected = null;
            this.xbttn.IconMarginLeft = 0;
            this.xbttn.IconMarginRight = 0;
            this.xbttn.IconRightVisible = true;
            this.xbttn.IconRightZoom = 0D;
            this.xbttn.IconVisible = true;
            this.xbttn.IconZoom = 90D;
            this.xbttn.IsTab = true;
            this.xbttn.Location = new System.Drawing.Point(1262, 0);
            this.xbttn.Name = "xbttn";
            this.xbttn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.xbttn.OnHovercolor = System.Drawing.Color.Maroon;
            this.xbttn.OnHoverTextColor = System.Drawing.Color.White;
            this.xbttn.selected = true;
            this.xbttn.Size = new System.Drawing.Size(46, 28);
            this.xbttn.TabIndex = 10;
            this.xbttn.Text = "Exit";
            this.xbttn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.xbttn.Textcolor = System.Drawing.Color.White;
            this.xbttn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.xbttn.Click += new System.EventHandler(this.BunifuFlatButton4_Click);
            // 
            // InfoLabel
            // 
            this.InfoLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.InfoLabel.AutoSize = true;
            this.IMGAni.SetDecoration(this.InfoLabel, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.InfoLabel, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.InfoLabel, BunifuAnimatorNS.DecorationType.None);
            this.InfoLabel.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.InfoLabel.ForeColor = System.Drawing.Color.DimGray;
            this.InfoLabel.Location = new System.Drawing.Point(2555, 3384);
            this.InfoLabel.Name = "InfoLabel";
            this.InfoLabel.Size = new System.Drawing.Size(184, 19);
            this.InfoLabel.TabIndex = 8;
            this.InfoLabel.Text = "Version X.X.X.X - Pre-Release";
            // 
            // GModPane
            // 
            this.GModPane.AutoScroll = true;
            this.GModPane.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.GModPane.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.GModPane.Controls.Add(this.mrp_infotab);
            this.GModPane.Controls.Add(this.panel1);
            this.AlertAni.SetDecoration(this.GModPane, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.GModPane, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.GModPane, BunifuAnimatorNS.DecorationType.None);
            this.GModPane.Location = new System.Drawing.Point(1262, 701);
            this.GModPane.Name = "GModPane";
            this.GModPane.Size = new System.Drawing.Size(46, 47);
            this.GModPane.TabIndex = 0;
            // 
            // mrp_infotab
            // 
            this.mrp_infotab.Controls.Add(this.axWebBrowser1);
            this.mrp_infotab.Controls.Add(this.nc1);
            this.AlertAni.SetDecoration(this.mrp_infotab, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.mrp_infotab, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.mrp_infotab, BunifuAnimatorNS.DecorationType.None);
            this.mrp_infotab.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mrp_infotab.Location = new System.Drawing.Point(200, 0);
            this.mrp_infotab.Name = "mrp_infotab";
            this.mrp_infotab.Size = new System.Drawing.Size(0, 30);
            this.mrp_infotab.TabIndex = 11;
            // 
            // axWebBrowser1
            // 
            this.axWebBrowser1.AllowWebBrowserDrop = false;
            this.MenuAni.SetDecoration(this.axWebBrowser1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.axWebBrowser1, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.axWebBrowser1, BunifuAnimatorNS.DecorationType.None);
            this.axWebBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axWebBrowser1.Location = new System.Drawing.Point(0, 0);
            this.axWebBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.axWebBrowser1.Name = "axWebBrowser1";
            this.axWebBrowser1.ScriptErrorsSuppressed = true;
            this.axWebBrowser1.Size = new System.Drawing.Size(20, 20);
            this.axWebBrowser1.TabIndex = 8;
            this.axWebBrowser1.Url = new System.Uri("https://f.monolithservers.com/forums/", System.UriKind.Absolute);
            this.axWebBrowser1.WebBrowserShortcutsEnabled = false;
            // 
            // nc1
            // 
            this.nc1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(43)))), ((int)(((byte)(47)))));
            this.nc1.BorderRadius = 5;
            this.nc1.BottomSahddow = true;
            this.nc1.color = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(43)))), ((int)(((byte)(47)))));
            this.nc1.Controls.Add(this.linkLabel1);
            this.nc1.Controls.Add(this.pictureBox2);
            this.nc1.Controls.Add(this.nb1);
            this.nc1.Controls.Add(this.nt1);
            this.AlertAni.SetDecoration(this.nc1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.nc1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.nc1, BunifuAnimatorNS.DecorationType.None);
            this.nc1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.nc1.LeftSahddow = false;
            this.nc1.Location = new System.Drawing.Point(0, -51);
            this.nc1.Name = "nc1";
            this.nc1.RightSahddow = true;
            this.nc1.ShadowDepth = 0;
            this.nc1.Size = new System.Drawing.Size(0, 81);
            this.nc1.TabIndex = 7;
            this.nc1.Visible = false;
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.MenuAni.SetDecoration(this.linkLabel1, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.linkLabel1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.linkLabel1, BunifuAnimatorNS.DecorationType.None);
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Location = new System.Drawing.Point(996, 13);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(16, 16);
            this.linkLabel1.TabIndex = 5;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "X";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel1_LinkClicked);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImage = global::Monolith_Launcher_2.Properties.Resources.plugin_50px;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MenuAni.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox2.Location = new System.Drawing.Point(26, 13);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(62, 55);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // nb1
            // 
            this.nb1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(43)))), ((int)(((byte)(47)))));
            this.nb1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AlertAni.SetDecoration(this.nb1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.nb1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.nb1, BunifuAnimatorNS.DecorationType.None);
            this.nb1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nb1.ForeColor = System.Drawing.Color.White;
            this.nb1.Location = new System.Drawing.Point(129, 36);
            this.nb1.Name = "nb1";
            this.nb1.ReadOnly = true;
            this.nb1.Size = new System.Drawing.Size(839, 34);
            this.nb1.TabIndex = 2;
            this.nb1.Text = "****.dll has been installed with this new update, review change-logs for more inf" +
    "ormation about this new extension.";
            // 
            // nt1
            // 
            this.nt1.AutoSize = true;
            this.IMGAni.SetDecoration(this.nt1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.nt1, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.nt1, BunifuAnimatorNS.DecorationType.None);
            this.nt1.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nt1.ForeColor = System.Drawing.Color.Lime;
            this.nt1.Location = new System.Drawing.Point(125, 9);
            this.nt1.Name = "nt1";
            this.nt1.Size = new System.Drawing.Size(264, 21);
            this.nt1.TabIndex = 1;
            this.nt1.Text = "A new extension has been installed";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.AlertAni.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 30);
            this.panel1.TabIndex = 10;
            // 
            // panel4
            // 
            this.panel4.AutoScroll = true;
            this.panel4.Controls.Add(this.SocialP);
            this.panel4.Controls.Add(this.CommunityP);
            this.panel4.Controls.Add(this.MRPP);
            this.panel4.Controls.Add(this.NewsNInfo);
            this.AlertAni.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.panel4, BunifuAnimatorNS.DecorationType.None);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 100);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(183, 0);
            this.panel4.TabIndex = 36;
            // 
            // SocialP
            // 
            this.SocialP.Controls.Add(this.linkLabel5);
            this.SocialP.Controls.Add(this.bunifuFlatButton28);
            this.SocialP.Controls.Add(this.bunifuFlatButton33);
            this.AlertAni.SetDecoration(this.SocialP, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.SocialP, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.SocialP, BunifuAnimatorNS.DecorationType.None);
            this.SocialP.Dock = System.Windows.Forms.DockStyle.Top;
            this.SocialP.Location = new System.Drawing.Point(0, 105);
            this.SocialP.Name = "SocialP";
            this.SocialP.Size = new System.Drawing.Size(183, 35);
            this.SocialP.TabIndex = 3;
            // 
            // linkLabel5
            // 
            this.linkLabel5.ActiveLinkColor = System.Drawing.Color.DarkGray;
            this.linkLabel5.AutoSize = true;
            this.MenuAni.SetDecoration(this.linkLabel5, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.linkLabel5, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.linkLabel5, BunifuAnimatorNS.DecorationType.None);
            this.linkLabel5.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.linkLabel5.ForeColor = System.Drawing.Color.Gray;
            this.linkLabel5.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabel5.LinkColor = System.Drawing.Color.LightGray;
            this.linkLabel5.Location = new System.Drawing.Point(8, 10);
            this.linkLabel5.Name = "linkLabel5";
            this.linkLabel5.Size = new System.Drawing.Size(167, 16);
            this.linkLabel5.TabIndex = 1;
            this.linkLabel5.TabStop = true;
            this.linkLabel5.Text = "► Social (External Links)";
            this.linkLabel5.UseMnemonic = false;
            this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel5_LinkClicked);
            // 
            // bunifuFlatButton28
            // 
            this.bunifuFlatButton28.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton28.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton28.BorderRadius = 0;
            this.bunifuFlatButton28.ButtonText = "Discord Server";
            this.bunifuFlatButton28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton28, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton28, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton28, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton28.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton28.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton28.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton28.Iconimage")));
            this.bunifuFlatButton28.Iconimage_right = null;
            this.bunifuFlatButton28.Iconimage_right_Selected = null;
            this.bunifuFlatButton28.Iconimage_Selected = null;
            this.bunifuFlatButton28.IconMarginLeft = 0;
            this.bunifuFlatButton28.IconMarginRight = 0;
            this.bunifuFlatButton28.IconRightVisible = true;
            this.bunifuFlatButton28.IconRightZoom = 0D;
            this.bunifuFlatButton28.IconVisible = true;
            this.bunifuFlatButton28.IconZoom = 65D;
            this.bunifuFlatButton28.IsTab = false;
            this.bunifuFlatButton28.Location = new System.Drawing.Point(11, 34);
            this.bunifuFlatButton28.Name = "bunifuFlatButton28";
            this.bunifuFlatButton28.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton28.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton28.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton28.selected = false;
            this.bunifuFlatButton28.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton28.TabIndex = 15;
            this.bunifuFlatButton28.Text = "Discord Server";
            this.bunifuFlatButton28.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton28.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton28.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton28.Click += new System.EventHandler(this.BunifuFlatButton28_Click);
            // 
            // bunifuFlatButton33
            // 
            this.bunifuFlatButton33.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton33.BorderRadius = 0;
            this.bunifuFlatButton33.ButtonText = "Steam Group";
            this.bunifuFlatButton33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton33, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton33, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton33, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton33.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton33.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton33.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton33.Iconimage")));
            this.bunifuFlatButton33.Iconimage_right = null;
            this.bunifuFlatButton33.Iconimage_right_Selected = null;
            this.bunifuFlatButton33.Iconimage_Selected = null;
            this.bunifuFlatButton33.IconMarginLeft = 0;
            this.bunifuFlatButton33.IconMarginRight = 0;
            this.bunifuFlatButton33.IconRightVisible = true;
            this.bunifuFlatButton33.IconRightZoom = 0D;
            this.bunifuFlatButton33.IconVisible = true;
            this.bunifuFlatButton33.IconZoom = 65D;
            this.bunifuFlatButton33.IsTab = false;
            this.bunifuFlatButton33.Location = new System.Drawing.Point(11, 70);
            this.bunifuFlatButton33.Name = "bunifuFlatButton33";
            this.bunifuFlatButton33.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton33.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton33.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton33.selected = false;
            this.bunifuFlatButton33.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton33.TabIndex = 20;
            this.bunifuFlatButton33.Text = "Steam Group";
            this.bunifuFlatButton33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton33.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton33.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton33.Click += new System.EventHandler(this.BunifuFlatButton33_Click);
            // 
            // CommunityP
            // 
            this.CommunityP.Controls.Add(this.bunifuFlatButton26);
            this.CommunityP.Controls.Add(this.linkLabel4);
            this.CommunityP.Controls.Add(this.bunifuFlatButton29);
            this.CommunityP.Controls.Add(this.bunifuFlatButton30);
            this.CommunityP.Controls.Add(this.bunifuFlatButton31);
            this.AlertAni.SetDecoration(this.CommunityP, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.CommunityP, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.CommunityP, BunifuAnimatorNS.DecorationType.None);
            this.CommunityP.Dock = System.Windows.Forms.DockStyle.Top;
            this.CommunityP.Location = new System.Drawing.Point(0, 70);
            this.CommunityP.Name = "CommunityP";
            this.CommunityP.Size = new System.Drawing.Size(183, 35);
            this.CommunityP.TabIndex = 2;
            // 
            // bunifuFlatButton26
            // 
            this.bunifuFlatButton26.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton26.BorderRadius = 0;
            this.bunifuFlatButton26.ButtonText = "Technical Support";
            this.bunifuFlatButton26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton26, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton26, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton26, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton26.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton26.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton26.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton26.Iconimage")));
            this.bunifuFlatButton26.Iconimage_right = null;
            this.bunifuFlatButton26.Iconimage_right_Selected = null;
            this.bunifuFlatButton26.Iconimage_Selected = null;
            this.bunifuFlatButton26.IconMarginLeft = 0;
            this.bunifuFlatButton26.IconMarginRight = 0;
            this.bunifuFlatButton26.IconRightVisible = true;
            this.bunifuFlatButton26.IconRightZoom = 0D;
            this.bunifuFlatButton26.IconVisible = true;
            this.bunifuFlatButton26.IconZoom = 65D;
            this.bunifuFlatButton26.IsTab = false;
            this.bunifuFlatButton26.Location = new System.Drawing.Point(11, 142);
            this.bunifuFlatButton26.Name = "bunifuFlatButton26";
            this.bunifuFlatButton26.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton26.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton26.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton26.selected = false;
            this.bunifuFlatButton26.Size = new System.Drawing.Size(167, 30);
            this.bunifuFlatButton26.TabIndex = 22;
            this.bunifuFlatButton26.Text = "Technical Support";
            this.bunifuFlatButton26.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton26.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton26.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // linkLabel4
            // 
            this.linkLabel4.ActiveLinkColor = System.Drawing.Color.DarkGray;
            this.linkLabel4.AutoSize = true;
            this.MenuAni.SetDecoration(this.linkLabel4, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.linkLabel4, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.linkLabel4, BunifuAnimatorNS.DecorationType.None);
            this.linkLabel4.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.linkLabel4.ForeColor = System.Drawing.Color.Gray;
            this.linkLabel4.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabel4.LinkColor = System.Drawing.Color.LightGray;
            this.linkLabel4.Location = new System.Drawing.Point(8, 10);
            this.linkLabel4.Name = "linkLabel4";
            this.linkLabel4.Size = new System.Drawing.Size(96, 16);
            this.linkLabel4.TabIndex = 1;
            this.linkLabel4.TabStop = true;
            this.linkLabel4.Text = "► Community";
            this.linkLabel4.UseMnemonic = false;
            this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel4_LinkClicked);
            // 
            // bunifuFlatButton29
            // 
            this.bunifuFlatButton29.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton29.BorderRadius = 0;
            this.bunifuFlatButton29.ButtonText = "General";
            this.bunifuFlatButton29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton29, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton29, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton29, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton29.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton29.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton29.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton29.Iconimage")));
            this.bunifuFlatButton29.Iconimage_right = null;
            this.bunifuFlatButton29.Iconimage_right_Selected = null;
            this.bunifuFlatButton29.Iconimage_Selected = null;
            this.bunifuFlatButton29.IconMarginLeft = 0;
            this.bunifuFlatButton29.IconMarginRight = 0;
            this.bunifuFlatButton29.IconRightVisible = true;
            this.bunifuFlatButton29.IconRightZoom = 0D;
            this.bunifuFlatButton29.IconVisible = true;
            this.bunifuFlatButton29.IconZoom = 65D;
            this.bunifuFlatButton29.IsTab = false;
            this.bunifuFlatButton29.Location = new System.Drawing.Point(11, 34);
            this.bunifuFlatButton29.Name = "bunifuFlatButton29";
            this.bunifuFlatButton29.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton29.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton29.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton29.selected = false;
            this.bunifuFlatButton29.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton29.TabIndex = 15;
            this.bunifuFlatButton29.Text = "General";
            this.bunifuFlatButton29.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton29.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton29.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton30
            // 
            this.bunifuFlatButton30.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton30.BorderRadius = 0;
            this.bunifuFlatButton30.ButtonText = "Television, Film, Music & Games";
            this.bunifuFlatButton30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton30, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton30, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton30, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton30.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton30.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton30.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton30.Iconimage")));
            this.bunifuFlatButton30.Iconimage_right = null;
            this.bunifuFlatButton30.Iconimage_right_Selected = null;
            this.bunifuFlatButton30.Iconimage_Selected = null;
            this.bunifuFlatButton30.IconMarginLeft = 0;
            this.bunifuFlatButton30.IconMarginRight = 0;
            this.bunifuFlatButton30.IconRightVisible = true;
            this.bunifuFlatButton30.IconRightZoom = 0D;
            this.bunifuFlatButton30.IconVisible = true;
            this.bunifuFlatButton30.IconZoom = 65D;
            this.bunifuFlatButton30.IsTab = false;
            this.bunifuFlatButton30.Location = new System.Drawing.Point(11, 106);
            this.bunifuFlatButton30.Name = "bunifuFlatButton30";
            this.bunifuFlatButton30.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton30.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton30.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton30.selected = false;
            this.bunifuFlatButton30.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton30.TabIndex = 21;
            this.bunifuFlatButton30.Text = "Television, Film, Music & Games";
            this.bunifuFlatButton30.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton30.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton30.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton31
            // 
            this.bunifuFlatButton31.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton31.BorderRadius = 0;
            this.bunifuFlatButton31.ButtonText = "Introductions";
            this.bunifuFlatButton31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton31, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton31, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton31, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton31.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton31.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton31.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton31.Iconimage")));
            this.bunifuFlatButton31.Iconimage_right = null;
            this.bunifuFlatButton31.Iconimage_right_Selected = null;
            this.bunifuFlatButton31.Iconimage_Selected = null;
            this.bunifuFlatButton31.IconMarginLeft = 0;
            this.bunifuFlatButton31.IconMarginRight = 0;
            this.bunifuFlatButton31.IconRightVisible = true;
            this.bunifuFlatButton31.IconRightZoom = 0D;
            this.bunifuFlatButton31.IconVisible = true;
            this.bunifuFlatButton31.IconZoom = 65D;
            this.bunifuFlatButton31.IsTab = false;
            this.bunifuFlatButton31.Location = new System.Drawing.Point(11, 70);
            this.bunifuFlatButton31.Name = "bunifuFlatButton31";
            this.bunifuFlatButton31.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton31.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton31.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton31.selected = false;
            this.bunifuFlatButton31.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton31.TabIndex = 20;
            this.bunifuFlatButton31.Text = "Introductions";
            this.bunifuFlatButton31.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton31.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton31.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // MRPP
            // 
            this.MRPP.Controls.Add(this.bunifuFlatButton23);
            this.MRPP.Controls.Add(this.bunifuFlatButton24);
            this.MRPP.Controls.Add(this.bunifuFlatButton25);
            this.MRPP.Controls.Add(this.linkLabel3);
            this.MRPP.Controls.Add(this.bunifuFlatButton6);
            this.MRPP.Controls.Add(this.bunifuFlatButton19);
            this.MRPP.Controls.Add(this.bunifuFlatButton22);
            this.AlertAni.SetDecoration(this.MRPP, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.MRPP, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.MRPP, BunifuAnimatorNS.DecorationType.None);
            this.MRPP.Dock = System.Windows.Forms.DockStyle.Top;
            this.MRPP.Location = new System.Drawing.Point(0, 35);
            this.MRPP.Name = "MRPP";
            this.MRPP.Size = new System.Drawing.Size(183, 35);
            this.MRPP.TabIndex = 1;
            // 
            // bunifuFlatButton23
            // 
            this.bunifuFlatButton23.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton23.BorderRadius = 0;
            this.bunifuFlatButton23.ButtonText = "Roleplay Documents";
            this.bunifuFlatButton23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton23, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton23, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton23, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton23.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton23.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton23.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton23.Iconimage")));
            this.bunifuFlatButton23.Iconimage_right = null;
            this.bunifuFlatButton23.Iconimage_right_Selected = null;
            this.bunifuFlatButton23.Iconimage_Selected = null;
            this.bunifuFlatButton23.IconMarginLeft = 0;
            this.bunifuFlatButton23.IconMarginRight = 0;
            this.bunifuFlatButton23.IconRightVisible = true;
            this.bunifuFlatButton23.IconRightZoom = 0D;
            this.bunifuFlatButton23.IconVisible = true;
            this.bunifuFlatButton23.IconZoom = 65D;
            this.bunifuFlatButton23.IsTab = false;
            this.bunifuFlatButton23.Location = new System.Drawing.Point(11, 142);
            this.bunifuFlatButton23.Name = "bunifuFlatButton23";
            this.bunifuFlatButton23.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton23.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton23.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton23.selected = false;
            this.bunifuFlatButton23.Size = new System.Drawing.Size(167, 30);
            this.bunifuFlatButton23.TabIndex = 22;
            this.bunifuFlatButton23.Text = "Roleplay Documents";
            this.bunifuFlatButton23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton23.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton23.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton23.Click += new System.EventHandler(this.BunifuFlatButton23_Click);
            // 
            // bunifuFlatButton24
            // 
            this.bunifuFlatButton24.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton24.BorderRadius = 0;
            this.bunifuFlatButton24.ButtonText = "Feedback";
            this.bunifuFlatButton24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton24, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton24, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton24, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton24.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton24.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton24.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton24.Iconimage")));
            this.bunifuFlatButton24.Iconimage_right = null;
            this.bunifuFlatButton24.Iconimage_right_Selected = null;
            this.bunifuFlatButton24.Iconimage_Selected = null;
            this.bunifuFlatButton24.IconMarginLeft = 0;
            this.bunifuFlatButton24.IconMarginRight = 0;
            this.bunifuFlatButton24.IconRightVisible = true;
            this.bunifuFlatButton24.IconRightZoom = 0D;
            this.bunifuFlatButton24.IconVisible = true;
            this.bunifuFlatButton24.IconZoom = 65D;
            this.bunifuFlatButton24.IsTab = false;
            this.bunifuFlatButton24.Location = new System.Drawing.Point(11, 214);
            this.bunifuFlatButton24.Name = "bunifuFlatButton24";
            this.bunifuFlatButton24.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton24.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton24.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton24.selected = false;
            this.bunifuFlatButton24.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton24.TabIndex = 24;
            this.bunifuFlatButton24.Text = "Feedback";
            this.bunifuFlatButton24.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton24.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton24.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton24.Click += new System.EventHandler(this.BunifuFlatButton24_Click);
            // 
            // bunifuFlatButton25
            // 
            this.bunifuFlatButton25.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton25.BorderRadius = 0;
            this.bunifuFlatButton25.ButtonText = "Refund Request";
            this.bunifuFlatButton25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton25, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton25, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton25, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton25.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton25.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton25.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton25.Iconimage")));
            this.bunifuFlatButton25.Iconimage_right = null;
            this.bunifuFlatButton25.Iconimage_right_Selected = null;
            this.bunifuFlatButton25.Iconimage_Selected = null;
            this.bunifuFlatButton25.IconMarginLeft = 0;
            this.bunifuFlatButton25.IconMarginRight = 0;
            this.bunifuFlatButton25.IconRightVisible = true;
            this.bunifuFlatButton25.IconRightZoom = 0D;
            this.bunifuFlatButton25.IconVisible = true;
            this.bunifuFlatButton25.IconZoom = 65D;
            this.bunifuFlatButton25.IsTab = false;
            this.bunifuFlatButton25.Location = new System.Drawing.Point(11, 178);
            this.bunifuFlatButton25.Name = "bunifuFlatButton25";
            this.bunifuFlatButton25.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton25.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton25.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton25.selected = false;
            this.bunifuFlatButton25.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton25.TabIndex = 23;
            this.bunifuFlatButton25.Text = "Refund Request";
            this.bunifuFlatButton25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton25.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton25.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton25.Click += new System.EventHandler(this.BunifuFlatButton25_Click);
            // 
            // linkLabel3
            // 
            this.linkLabel3.ActiveLinkColor = System.Drawing.Color.DarkGray;
            this.linkLabel3.AutoSize = true;
            this.MenuAni.SetDecoration(this.linkLabel3, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.linkLabel3, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.linkLabel3, BunifuAnimatorNS.DecorationType.None);
            this.linkLabel3.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.linkLabel3.ForeColor = System.Drawing.Color.Gray;
            this.linkLabel3.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabel3.LinkColor = System.Drawing.Color.LightGray;
            this.linkLabel3.Location = new System.Drawing.Point(8, 10);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(101, 16);
            this.linkLabel3.TabIndex = 1;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "► Monolith RP";
            this.linkLabel3.UseMnemonic = false;
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel3_LinkClicked);
            // 
            // bunifuFlatButton6
            // 
            this.bunifuFlatButton6.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton6.BorderRadius = 0;
            this.bunifuFlatButton6.ButtonText = "Discussions";
            this.bunifuFlatButton6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton6, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton6, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton6.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton6.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton6.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton6.Iconimage")));
            this.bunifuFlatButton6.Iconimage_right = null;
            this.bunifuFlatButton6.Iconimage_right_Selected = null;
            this.bunifuFlatButton6.Iconimage_Selected = null;
            this.bunifuFlatButton6.IconMarginLeft = 0;
            this.bunifuFlatButton6.IconMarginRight = 0;
            this.bunifuFlatButton6.IconRightVisible = true;
            this.bunifuFlatButton6.IconRightZoom = 0D;
            this.bunifuFlatButton6.IconVisible = true;
            this.bunifuFlatButton6.IconZoom = 65D;
            this.bunifuFlatButton6.IsTab = false;
            this.bunifuFlatButton6.Location = new System.Drawing.Point(11, 34);
            this.bunifuFlatButton6.Name = "bunifuFlatButton6";
            this.bunifuFlatButton6.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton6.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton6.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton6.selected = false;
            this.bunifuFlatButton6.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton6.TabIndex = 15;
            this.bunifuFlatButton6.Text = "Discussions";
            this.bunifuFlatButton6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton6.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton6.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton6.Click += new System.EventHandler(this.BunifuFlatButton6_Click_1);
            // 
            // bunifuFlatButton19
            // 
            this.bunifuFlatButton19.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton19.BorderRadius = 0;
            this.bunifuFlatButton19.ButtonText = "Guides";
            this.bunifuFlatButton19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton19, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton19, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton19, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton19.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton19.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton19.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton19.Iconimage")));
            this.bunifuFlatButton19.Iconimage_right = null;
            this.bunifuFlatButton19.Iconimage_right_Selected = null;
            this.bunifuFlatButton19.Iconimage_Selected = null;
            this.bunifuFlatButton19.IconMarginLeft = 0;
            this.bunifuFlatButton19.IconMarginRight = 0;
            this.bunifuFlatButton19.IconRightVisible = true;
            this.bunifuFlatButton19.IconRightZoom = 0D;
            this.bunifuFlatButton19.IconVisible = true;
            this.bunifuFlatButton19.IconZoom = 65D;
            this.bunifuFlatButton19.IsTab = false;
            this.bunifuFlatButton19.Location = new System.Drawing.Point(11, 106);
            this.bunifuFlatButton19.Name = "bunifuFlatButton19";
            this.bunifuFlatButton19.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton19.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton19.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton19.selected = false;
            this.bunifuFlatButton19.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton19.TabIndex = 21;
            this.bunifuFlatButton19.Text = "Guides";
            this.bunifuFlatButton19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton19.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton19.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton19.Click += new System.EventHandler(this.BunifuFlatButton19_Click);
            // 
            // bunifuFlatButton22
            // 
            this.bunifuFlatButton22.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton22.BorderRadius = 0;
            this.bunifuFlatButton22.ButtonText = "Events";
            this.bunifuFlatButton22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton22, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton22, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton22, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton22.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton22.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton22.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton22.Iconimage")));
            this.bunifuFlatButton22.Iconimage_right = null;
            this.bunifuFlatButton22.Iconimage_right_Selected = null;
            this.bunifuFlatButton22.Iconimage_Selected = null;
            this.bunifuFlatButton22.IconMarginLeft = 0;
            this.bunifuFlatButton22.IconMarginRight = 0;
            this.bunifuFlatButton22.IconRightVisible = true;
            this.bunifuFlatButton22.IconRightZoom = 0D;
            this.bunifuFlatButton22.IconVisible = true;
            this.bunifuFlatButton22.IconZoom = 65D;
            this.bunifuFlatButton22.IsTab = false;
            this.bunifuFlatButton22.Location = new System.Drawing.Point(11, 70);
            this.bunifuFlatButton22.Name = "bunifuFlatButton22";
            this.bunifuFlatButton22.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton22.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton22.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton22.selected = false;
            this.bunifuFlatButton22.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton22.TabIndex = 20;
            this.bunifuFlatButton22.Text = "Events";
            this.bunifuFlatButton22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton22.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton22.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton22.Click += new System.EventHandler(this.BunifuFlatButton22_Click);
            // 
            // NewsNInfo
            // 
            this.NewsNInfo.Controls.Add(this.linkLabel2);
            this.NewsNInfo.Controls.Add(this.bunifuFlatButton7);
            this.NewsNInfo.Controls.Add(this.bunifuFlatButton9);
            this.NewsNInfo.Controls.Add(this.bunifuFlatButton8);
            this.AlertAni.SetDecoration(this.NewsNInfo, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.NewsNInfo, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.NewsNInfo, BunifuAnimatorNS.DecorationType.None);
            this.NewsNInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.NewsNInfo.Location = new System.Drawing.Point(0, 0);
            this.NewsNInfo.Name = "NewsNInfo";
            this.NewsNInfo.Size = new System.Drawing.Size(183, 35);
            this.NewsNInfo.TabIndex = 0;
            // 
            // linkLabel2
            // 
            this.linkLabel2.ActiveLinkColor = System.Drawing.Color.DarkGray;
            this.linkLabel2.AutoSize = true;
            this.MenuAni.SetDecoration(this.linkLabel2, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.linkLabel2, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.linkLabel2, BunifuAnimatorNS.DecorationType.None);
            this.linkLabel2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold);
            this.linkLabel2.ForeColor = System.Drawing.Color.Gray;
            this.linkLabel2.LinkBehavior = System.Windows.Forms.LinkBehavior.HoverUnderline;
            this.linkLabel2.LinkColor = System.Drawing.Color.LightGray;
            this.linkLabel2.Location = new System.Drawing.Point(8, 10);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(154, 16);
            this.linkLabel2.TabIndex = 1;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "► News & Information";
            this.linkLabel2.UseMnemonic = false;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.LinkLabel2_LinkClicked);
            // 
            // bunifuFlatButton7
            // 
            this.bunifuFlatButton7.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton7.BorderRadius = 0;
            this.bunifuFlatButton7.ButtonText = "Announcements";
            this.bunifuFlatButton7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton7, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton7, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton7, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton7.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton7.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton7.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton7.Iconimage")));
            this.bunifuFlatButton7.Iconimage_right = null;
            this.bunifuFlatButton7.Iconimage_right_Selected = null;
            this.bunifuFlatButton7.Iconimage_Selected = null;
            this.bunifuFlatButton7.IconMarginLeft = 0;
            this.bunifuFlatButton7.IconMarginRight = 0;
            this.bunifuFlatButton7.IconRightVisible = true;
            this.bunifuFlatButton7.IconRightZoom = 0D;
            this.bunifuFlatButton7.IconVisible = true;
            this.bunifuFlatButton7.IconZoom = 65D;
            this.bunifuFlatButton7.IsTab = false;
            this.bunifuFlatButton7.Location = new System.Drawing.Point(11, 32);
            this.bunifuFlatButton7.Name = "bunifuFlatButton7";
            this.bunifuFlatButton7.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton7.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton7.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton7.selected = false;
            this.bunifuFlatButton7.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton7.TabIndex = 15;
            this.bunifuFlatButton7.Text = "Announcements";
            this.bunifuFlatButton7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton7.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton7.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton7.Click += new System.EventHandler(this.BunifuFlatButton7_Click);
            // 
            // bunifuFlatButton9
            // 
            this.bunifuFlatButton9.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton9.BorderRadius = 0;
            this.bunifuFlatButton9.ButtonText = "Updates";
            this.bunifuFlatButton9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton9, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton9, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton9, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton9.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton9.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton9.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton9.Iconimage")));
            this.bunifuFlatButton9.Iconimage_right = null;
            this.bunifuFlatButton9.Iconimage_right_Selected = null;
            this.bunifuFlatButton9.Iconimage_Selected = null;
            this.bunifuFlatButton9.IconMarginLeft = 0;
            this.bunifuFlatButton9.IconMarginRight = 0;
            this.bunifuFlatButton9.IconRightVisible = true;
            this.bunifuFlatButton9.IconRightZoom = 0D;
            this.bunifuFlatButton9.IconVisible = true;
            this.bunifuFlatButton9.IconZoom = 65D;
            this.bunifuFlatButton9.IsTab = false;
            this.bunifuFlatButton9.Location = new System.Drawing.Point(11, 104);
            this.bunifuFlatButton9.Name = "bunifuFlatButton9";
            this.bunifuFlatButton9.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton9.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton9.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton9.selected = false;
            this.bunifuFlatButton9.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton9.TabIndex = 21;
            this.bunifuFlatButton9.Text = "Updates";
            this.bunifuFlatButton9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton9.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton9.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton9.Click += new System.EventHandler(this.BunifuFlatButton9_Click);
            // 
            // bunifuFlatButton8
            // 
            this.bunifuFlatButton8.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton8.BorderRadius = 0;
            this.bunifuFlatButton8.ButtonText = "Rules - Information";
            this.bunifuFlatButton8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton8, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton8, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton8.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton8.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton8.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton8.Iconimage")));
            this.bunifuFlatButton8.Iconimage_right = null;
            this.bunifuFlatButton8.Iconimage_right_Selected = null;
            this.bunifuFlatButton8.Iconimage_Selected = null;
            this.bunifuFlatButton8.IconMarginLeft = 0;
            this.bunifuFlatButton8.IconMarginRight = 0;
            this.bunifuFlatButton8.IconRightVisible = true;
            this.bunifuFlatButton8.IconRightZoom = 0D;
            this.bunifuFlatButton8.IconVisible = true;
            this.bunifuFlatButton8.IconZoom = 65D;
            this.bunifuFlatButton8.IsTab = false;
            this.bunifuFlatButton8.Location = new System.Drawing.Point(11, 68);
            this.bunifuFlatButton8.Name = "bunifuFlatButton8";
            this.bunifuFlatButton8.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton8.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton8.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton8.selected = false;
            this.bunifuFlatButton8.Size = new System.Drawing.Size(157, 30);
            this.bunifuFlatButton8.TabIndex = 20;
            this.bunifuFlatButton8.Text = "Rules - Information";
            this.bunifuFlatButton8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton8.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton8.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton8.Click += new System.EventHandler(this.BunifuFlatButton8_Click);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(43)))), ((int)(((byte)(47)))));
            this.panel3.Controls.Add(this.playbttn);
            this.panel3.Controls.Add(this.bunifuFlatButton5);
            this.AlertAni.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(0, 100);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(183, 81);
            this.panel3.TabIndex = 19;
            // 
            // playbttn
            // 
            this.playbttn.Activecolor = System.Drawing.Color.Transparent;
            this.playbttn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.playbttn.BackColor = System.Drawing.Color.Transparent;
            this.playbttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.playbttn.BorderRadius = 0;
            this.playbttn.ButtonText = "Launch Monolith";
            this.playbttn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.playbttn, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.playbttn, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.playbttn, BunifuAnimatorNS.DecorationType.None);
            this.playbttn.DisabledColor = System.Drawing.Color.Gray;
            this.playbttn.Iconcolor = System.Drawing.Color.Transparent;
            this.playbttn.Iconimage = global::Monolith_Launcher_2.Properties.Resources.play_24px;
            this.playbttn.Iconimage_right = null;
            this.playbttn.Iconimage_right_Selected = null;
            this.playbttn.Iconimage_Selected = null;
            this.playbttn.IconMarginLeft = 0;
            this.playbttn.IconMarginRight = 0;
            this.playbttn.IconRightVisible = true;
            this.playbttn.IconRightZoom = 0D;
            this.playbttn.IconVisible = true;
            this.playbttn.IconZoom = 50D;
            this.playbttn.IsTab = false;
            this.playbttn.Location = new System.Drawing.Point(33, 45);
            this.playbttn.Name = "playbttn";
            this.playbttn.Normalcolor = System.Drawing.Color.Transparent;
            this.playbttn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.playbttn.OnHoverTextColor = System.Drawing.Color.Silver;
            this.playbttn.selected = false;
            this.playbttn.Size = new System.Drawing.Size(134, 28);
            this.playbttn.TabIndex = 0;
            this.playbttn.Text = "Launch Monolith";
            this.playbttn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.playbttn.Textcolor = System.Drawing.Color.White;
            this.playbttn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.playbttn.Click += new System.EventHandler(this.BunifuFlatButton1_Click);
            // 
            // bunifuFlatButton5
            // 
            this.bunifuFlatButton5.Activecolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.bunifuFlatButton5.BackColor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton5.BorderRadius = 0;
            this.bunifuFlatButton5.ButtonText = "Collection Page";
            this.bunifuFlatButton5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton5.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton5.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.Iconimage = global::Monolith_Launcher_2.Properties.Resources.plugin_50px;
            this.bunifuFlatButton5.Iconimage_right = null;
            this.bunifuFlatButton5.Iconimage_right_Selected = null;
            this.bunifuFlatButton5.Iconimage_Selected = null;
            this.bunifuFlatButton5.IconMarginLeft = 0;
            this.bunifuFlatButton5.IconMarginRight = 0;
            this.bunifuFlatButton5.IconRightVisible = true;
            this.bunifuFlatButton5.IconRightZoom = 0D;
            this.bunifuFlatButton5.IconVisible = true;
            this.bunifuFlatButton5.IconZoom = 75D;
            this.bunifuFlatButton5.IsTab = false;
            this.bunifuFlatButton5.Location = new System.Drawing.Point(33, 8);
            this.bunifuFlatButton5.Name = "bunifuFlatButton5";
            this.bunifuFlatButton5.Normalcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton5.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(52)))), ((int)(((byte)(55)))), ((int)(((byte)(60)))));
            this.bunifuFlatButton5.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton5.selected = false;
            this.bunifuFlatButton5.Size = new System.Drawing.Size(134, 28);
            this.bunifuFlatButton5.TabIndex = 6;
            this.bunifuFlatButton5.Text = "Collection Page";
            this.bunifuFlatButton5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton5.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton5.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton5.Click += new System.EventHandler(this.BunifuFlatButton5_Click);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(41)))), ((int)(((byte)(43)))), ((int)(((byte)(47)))));
            this.panel2.BackgroundImage = global::Monolith_Launcher_2.Properties.Resources._20191114201429_1;
            this.panel2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel2.Controls.Add(this.pictureBox1);
            this.AlertAni.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(183, 100);
            this.panel2.TabIndex = 16;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImage = global::Monolith_Launcher_2.Properties.Resources.K20GBvj;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MenuAni.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.pictureBox1, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox1.Location = new System.Drawing.Point(11, 11);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(179, 73);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // SettingsPane
            // 
            this.SettingsPane.AutoScroll = true;
            this.SettingsPane.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SettingsPane.Controls.Add(this.panel5);
            this.SettingsPane.Controls.Add(this.label9);
            this.SettingsPane.Controls.Add(this.label10);
            this.SettingsPane.Controls.Add(this.InfoLabel);
            this.SettingsPane.Controls.Add(this.panel6);
            this.AlertAni.SetDecoration(this.SettingsPane, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.SettingsPane, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.SettingsPane, BunifuAnimatorNS.DecorationType.None);
            this.SettingsPane.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SettingsPane.Location = new System.Drawing.Point(66, 28);
            this.SettingsPane.Name = "SettingsPane";
            this.SettingsPane.Size = new System.Drawing.Size(1242, 720);
            this.SettingsPane.TabIndex = 7;
            this.SettingsPane.Visible = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.BetaCard1);
            this.panel5.Controls.Add(this.UpdateCard1);
            this.panel5.Controls.Add(this.ExtensionCard1);
            this.AlertAni.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.panel5, BunifuAnimatorNS.DecorationType.None);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(199, 0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(1043, 720);
            this.panel5.TabIndex = 17;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label9.AutoSize = true;
            this.IMGAni.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label9, BunifuAnimatorNS.DecorationType.None);
            this.label9.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(139, 1400);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(338, 25);
            this.label9.TabIndex = 14;
            this.label9.Text = "Modify Configurations for Monolith:RP";
            this.label9.Visible = false;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label10.AutoSize = true;
            this.IMGAni.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label10, BunifuAnimatorNS.DecorationType.None);
            this.label10.Font = new System.Drawing.Font("Segoe UI Semibold", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(136, 1363);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(170, 37);
            this.label10.TabIndex = 15;
            this.label10.Text = "Monolith:RP";
            this.label10.Visible = false;
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.bunifuFlatButton12);
            this.panel6.Controls.Add(this.bunifuFlatButton11);
            this.panel6.Controls.Add(this.bunifuFlatButton10);
            this.AlertAni.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.panel6, BunifuAnimatorNS.DecorationType.None);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(199, 720);
            this.panel6.TabIndex = 18;
            // 
            // BetaCard1
            // 
            this.BetaCard1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.BetaCard1.BorderRadius = 5;
            this.BetaCard1.BottomSahddow = true;
            this.BetaCard1.color = System.Drawing.Color.Purple;
            this.BetaCard1.Controls.Add(this.bunifuCards2);
            this.BetaCard1.Controls.Add(this.bunifuCards1);
            this.BetaCard1.Controls.Add(this.label1);
            this.BetaCard1.Controls.Add(this.bunifuSeparator3);
            this.BetaCard1.Controls.Add(this.label6);
            this.AlertAni.SetDecoration(this.BetaCard1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.BetaCard1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.BetaCard1, BunifuAnimatorNS.DecorationType.None);
            this.BetaCard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BetaCard1.LeftSahddow = false;
            this.BetaCard1.Location = new System.Drawing.Point(0, 0);
            this.BetaCard1.Name = "BetaCard1";
            this.BetaCard1.RightSahddow = true;
            this.BetaCard1.ShadowDepth = 20;
            this.BetaCard1.Size = new System.Drawing.Size(1043, 720);
            this.BetaCard1.TabIndex = 11;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.IMGAni.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label8, BunifuAnimatorNS.DecorationType.None);
            this.label8.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(29, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(157, 21);
            this.label8.TabIndex = 12;
            this.label8.Text = "Window Full Screen ";
            // 
            // fsbs
            // 
            this.fsbs.BackColor = System.Drawing.Color.Transparent;
            this.fsbs.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("fsbs.BackgroundImage")));
            this.fsbs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.fsbs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.fsbs, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.fsbs, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.fsbs, BunifuAnimatorNS.DecorationType.None);
            this.fsbs.Location = new System.Drawing.Point(207, 77);
            this.fsbs.Name = "fsbs";
            this.fsbs.OffColor = System.Drawing.Color.Gray;
            this.fsbs.OnColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(202)))), ((int)(((byte)(94)))));
            this.fsbs.Size = new System.Drawing.Size(43, 25);
            this.fsbs.TabIndex = 3;
            this.fsbs.Value = true;
            this.fsbs.OnValueChange += new System.EventHandler(this.BunifuiOSSwitch1_OnValueChange_1);
            // 
            // bunifuSeparator3
            // 
            this.bunifuSeparator3.BackColor = System.Drawing.Color.Transparent;
            this.MenuAni.SetDecoration(this.bunifuSeparator3, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuSeparator3, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuSeparator3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator3.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSeparator3.LineThickness = 1;
            this.bunifuSeparator3.Location = new System.Drawing.Point(22, 69);
            this.bunifuSeparator3.Name = "bunifuSeparator3";
            this.bunifuSeparator3.Size = new System.Drawing.Size(998, 35);
            this.bunifuSeparator3.TabIndex = 8;
            this.bunifuSeparator3.Transparency = 255;
            this.bunifuSeparator3.Vertical = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.IMGAni.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label6, BunifuAnimatorNS.DecorationType.None);
            this.label6.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.label6.Location = new System.Drawing.Point(17, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(212, 25);
            this.label6.TabIndex = 7;
            this.label6.Text = "Beta Features Available!";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ExtensionCard1
            // 
            this.ExtensionCard1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.ExtensionCard1.BorderRadius = 5;
            this.ExtensionCard1.BottomSahddow = true;
            this.ExtensionCard1.color = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.ExtensionCard1.Controls.Add(this.bunifuCards5);
            this.ExtensionCard1.Controls.Add(this.bunifuCards4);
            this.ExtensionCard1.Controls.Add(this.label18);
            this.ExtensionCard1.Controls.Add(this.bunifuSeparator2);
            this.ExtensionCard1.Controls.Add(this.label7);
            this.AlertAni.SetDecoration(this.ExtensionCard1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.ExtensionCard1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.ExtensionCard1, BunifuAnimatorNS.DecorationType.None);
            this.ExtensionCard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ExtensionCard1.LeftSahddow = false;
            this.ExtensionCard1.Location = new System.Drawing.Point(0, 0);
            this.ExtensionCard1.Name = "ExtensionCard1";
            this.ExtensionCard1.RightSahddow = true;
            this.ExtensionCard1.ShadowDepth = 20;
            this.ExtensionCard1.Size = new System.Drawing.Size(1043, 720);
            this.ExtensionCard1.TabIndex = 10;
            this.ExtensionCard1.Visible = false;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "checkmark_52px.png");
            this.imageList1.Images.SetKeyName(1, "link_52px.png");
            this.imageList1.Images.SetKeyName(2, "plugin_50px.png");
            this.imageList1.Images.SetKeyName(3, "services_filled_50px.png");
            // 
            // bunifuSeparator2
            // 
            this.bunifuSeparator2.BackColor = System.Drawing.Color.Transparent;
            this.MenuAni.SetDecoration(this.bunifuSeparator2, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuSeparator2, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuSeparator2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator2.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSeparator2.LineThickness = 1;
            this.bunifuSeparator2.Location = new System.Drawing.Point(22, 59);
            this.bunifuSeparator2.Name = "bunifuSeparator2";
            this.bunifuSeparator2.Size = new System.Drawing.Size(998, 35);
            this.bunifuSeparator2.TabIndex = 8;
            this.bunifuSeparator2.Transparency = 255;
            this.bunifuSeparator2.Vertical = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.IMGAni.SetDecoration(this.label7, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label7, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label7, BunifuAnimatorNS.DecorationType.None);
            this.label7.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(17, 11);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 25);
            this.label7.TabIndex = 7;
            this.label7.Text = "Extensions";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // UpdateCard1
            // 
            this.UpdateCard1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(54)))), ((int)(((byte)(57)))), ((int)(((byte)(63)))));
            this.UpdateCard1.BorderRadius = 5;
            this.UpdateCard1.BottomSahddow = true;
            this.UpdateCard1.color = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.UpdateCard1.Controls.Add(this.bunifuCards3);
            this.UpdateCard1.Controls.Add(this.label5);
            this.UpdateCard1.Controls.Add(this.bunifuSeparator1);
            this.UpdateCard1.Controls.Add(this.label4);
            this.AlertAni.SetDecoration(this.UpdateCard1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.UpdateCard1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.UpdateCard1, BunifuAnimatorNS.DecorationType.None);
            this.UpdateCard1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.UpdateCard1.LeftSahddow = false;
            this.UpdateCard1.Location = new System.Drawing.Point(0, 0);
            this.UpdateCard1.Name = "UpdateCard1";
            this.UpdateCard1.RightSahddow = true;
            this.UpdateCard1.ShadowDepth = 20;
            this.UpdateCard1.Size = new System.Drawing.Size(1043, 720);
            this.UpdateCard1.TabIndex = 8;
            this.UpdateCard1.Visible = false;
            // 
            // bunifuFlatButton1
            // 
            this.bunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton1.BorderRadius = 0;
            this.bunifuFlatButton1.ButtonText = "Check";
            this.bunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton1.Iconimage = null;
            this.bunifuFlatButton1.Iconimage_right = null;
            this.bunifuFlatButton1.Iconimage_right_Selected = null;
            this.bunifuFlatButton1.Iconimage_Selected = null;
            this.bunifuFlatButton1.IconMarginLeft = 0;
            this.bunifuFlatButton1.IconMarginRight = 0;
            this.bunifuFlatButton1.IconRightVisible = true;
            this.bunifuFlatButton1.IconRightZoom = 0D;
            this.bunifuFlatButton1.IconVisible = true;
            this.bunifuFlatButton1.IconZoom = 50D;
            this.bunifuFlatButton1.IsTab = false;
            this.bunifuFlatButton1.Location = new System.Drawing.Point(296, 142);
            this.bunifuFlatButton1.Name = "bunifuFlatButton1";
            this.bunifuFlatButton1.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton1.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton1.selected = false;
            this.bunifuFlatButton1.Size = new System.Drawing.Size(83, 35);
            this.bunifuFlatButton1.TabIndex = 13;
            this.bunifuFlatButton1.Text = "Check";
            this.bunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton1.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton1.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton1.Click += new System.EventHandler(this.BunifuFlatButton1_Click_1);
            // 
            // bunifuSeparator1
            // 
            this.bunifuSeparator1.BackColor = System.Drawing.Color.Transparent;
            this.MenuAni.SetDecoration(this.bunifuSeparator1, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuSeparator1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuSeparator1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator1.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSeparator1.LineThickness = 1;
            this.bunifuSeparator1.Location = new System.Drawing.Point(22, 59);
            this.bunifuSeparator1.Name = "bunifuSeparator1";
            this.bunifuSeparator1.Size = new System.Drawing.Size(998, 35);
            this.bunifuSeparator1.TabIndex = 8;
            this.bunifuSeparator1.Transparency = 255;
            this.bunifuSeparator1.Vertical = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.IMGAni.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label4, BunifuAnimatorNS.DecorationType.None);
            this.label4.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(17, 11);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(134, 25);
            this.label4.TabIndex = 7;
            this.label4.Text = "Update Config";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // IMGAni
            // 
            this.IMGAni.AnimationType = BunifuAnimatorNS.AnimationType.Transparent;
            this.IMGAni.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 1F;
            this.IMGAni.DefaultAnimation = animation1;
            this.IMGAni.Interval = 15;
            this.IMGAni.MaxAnimationTime = 500;
            // 
            // SideMenu
            // 
            this.SideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.SideMenu.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SideMenu.Controls.Add(this.bunifuCircleProgressbar1);
            this.SideMenu.Controls.Add(this.label11);
            this.SideMenu.Controls.Add(this.MonoInfoBttn);
            this.SideMenu.Controls.Add(this.bunifuFlatButton2);
            this.AlertAni.SetDecoration(this.SideMenu, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.SideMenu, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.SideMenu, BunifuAnimatorNS.DecorationType.None);
            this.SideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.SideMenu.Location = new System.Drawing.Point(0, 28);
            this.SideMenu.Name = "SideMenu";
            this.SideMenu.Size = new System.Drawing.Size(66, 720);
            this.SideMenu.TabIndex = 1;
            // 
            // bunifuCircleProgressbar1
            // 
            this.bunifuCircleProgressbar1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.bunifuCircleProgressbar1.animated = true;
            this.bunifuCircleProgressbar1.animationIterval = 5;
            this.bunifuCircleProgressbar1.animationSpeed = 1;
            this.bunifuCircleProgressbar1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuCircleProgressbar1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuCircleProgressbar1.BackgroundImage")));
            this.MenuAni.SetDecoration(this.bunifuCircleProgressbar1, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuCircleProgressbar1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuCircleProgressbar1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCircleProgressbar1.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F);
            this.bunifuCircleProgressbar1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.bunifuCircleProgressbar1.LabelVisible = false;
            this.bunifuCircleProgressbar1.LineProgressThickness = 8;
            this.bunifuCircleProgressbar1.LineThickness = 5;
            this.bunifuCircleProgressbar1.Location = new System.Drawing.Point(10, 644);
            this.bunifuCircleProgressbar1.Margin = new System.Windows.Forms.Padding(10, 9, 10, 9);
            this.bunifuCircleProgressbar1.MaxValue = 100;
            this.bunifuCircleProgressbar1.Name = "bunifuCircleProgressbar1";
            this.bunifuCircleProgressbar1.ProgressBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuCircleProgressbar1.ProgressColor = System.Drawing.Color.LightGray;
            this.bunifuCircleProgressbar1.Size = new System.Drawing.Size(46, 46);
            this.bunifuCircleProgressbar1.TabIndex = 4;
            this.bunifuCircleProgressbar1.Value = 65;
            this.bunifuCircleProgressbar1.Visible = false;
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label11.AutoSize = true;
            this.IMGAni.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label11, BunifuAnimatorNS.DecorationType.None);
            this.label11.ForeColor = System.Drawing.Color.DimGray;
            this.label11.Location = new System.Drawing.Point(11, 699);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "V. 1.1.0";
            // 
            // MonoInfoBttn
            // 
            this.MonoInfoBttn.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.MonoInfoBttn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.MonoInfoBttn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.MonoInfoBttn.BorderRadius = 0;
            this.MonoInfoBttn.ButtonText = "";
            this.MonoInfoBttn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.MonoInfoBttn, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.MonoInfoBttn, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.MonoInfoBttn, BunifuAnimatorNS.DecorationType.None);
            this.MonoInfoBttn.DisabledColor = System.Drawing.Color.Gray;
            this.MonoInfoBttn.Iconcolor = System.Drawing.Color.Transparent;
            this.MonoInfoBttn.Iconimage = global::Monolith_Launcher_2.Properties.Resources.Untitled;
            this.MonoInfoBttn.Iconimage_right = null;
            this.MonoInfoBttn.Iconimage_right_Selected = null;
            this.MonoInfoBttn.Iconimage_Selected = null;
            this.MonoInfoBttn.IconMarginLeft = 0;
            this.MonoInfoBttn.IconMarginRight = 0;
            this.MonoInfoBttn.IconRightVisible = true;
            this.MonoInfoBttn.IconRightZoom = 0D;
            this.MonoInfoBttn.IconVisible = true;
            this.MonoInfoBttn.IconZoom = 75D;
            this.MonoInfoBttn.IsTab = true;
            this.MonoInfoBttn.Location = new System.Drawing.Point(10, 65);
            this.MonoInfoBttn.Name = "MonoInfoBttn";
            this.MonoInfoBttn.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.MonoInfoBttn.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(42)))), ((int)(((byte)(46)))));
            this.MonoInfoBttn.OnHoverTextColor = System.Drawing.Color.Silver;
            this.MonoInfoBttn.selected = true;
            this.MonoInfoBttn.Size = new System.Drawing.Size(47, 48);
            this.MonoInfoBttn.TabIndex = 4;
            this.MonoInfoBttn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.MonoInfoBttn.Textcolor = System.Drawing.Color.White;
            this.MonoInfoBttn.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MonoInfoBttn.Click += new System.EventHandler(this.BunifuFlatButton3_Click);
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = "";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = global::Monolith_Launcher_2.Properties.Resources.job_50px;
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = true;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = true;
            this.bunifuFlatButton2.IconZoom = 75D;
            this.bunifuFlatButton2.IsTab = true;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(10, 11);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(34)))), ((int)(((byte)(37)))));
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(42)))), ((int)(((byte)(46)))));
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(47, 48);
            this.bunifuFlatButton2.TabIndex = 3;
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton2.Click += new System.EventHandler(this.BunifuFlatButton2_Click);
            // 
            // MenuAni
            // 
            this.MenuAni.AnimationType = BunifuAnimatorNS.AnimationType.Transparent;
            this.MenuAni.Cursor = null;
            animation2.AnimateOnlyDifferences = true;
            animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
            animation2.LeafCoeff = 0F;
            animation2.MaxTime = 1F;
            animation2.MinTime = 0F;
            animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
            animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
            animation2.MosaicSize = 0;
            animation2.Padding = new System.Windows.Forms.Padding(0);
            animation2.RotateCoeff = 0F;
            animation2.RotateLimit = 0F;
            animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
            animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
            animation2.TimeCoeff = 0F;
            animation2.TransparencyCoeff = 1F;
            this.MenuAni.DefaultAnimation = animation2;
            this.MenuAni.Interval = 5;
            this.MenuAni.MaxAnimationTime = 500;
            // 
            // processchk
            // 
            this.processchk.Enabled = true;
            this.processchk.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // AlertAni
            // 
            this.AlertAni.AnimationType = BunifuAnimatorNS.AnimationType.VertSlide;
            this.AlertAni.Cursor = null;
            animation3.AnimateOnlyDifferences = true;
            animation3.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.BlindCoeff")));
            animation3.LeafCoeff = 0F;
            animation3.MaxTime = 1F;
            animation3.MinTime = 0F;
            animation3.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicCoeff")));
            animation3.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation3.MosaicShift")));
            animation3.MosaicSize = 0;
            animation3.Padding = new System.Windows.Forms.Padding(0);
            animation3.RotateCoeff = 0F;
            animation3.RotateLimit = 0F;
            animation3.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.ScaleCoeff")));
            animation3.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation3.SlideCoeff")));
            animation3.TimeCoeff = 0F;
            animation3.TransparencyCoeff = 0F;
            this.AlertAni.DefaultAnimation = animation3;
            // 
            // crashchk
            // 
            this.crashchk.Enabled = true;
            this.crashchk.Tick += new System.EventHandler(this.Crashchk_Tick);
            // 
            // bunifuFlatButton10
            // 
            this.bunifuFlatButton10.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton10.BorderRadius = 0;
            this.bunifuFlatButton10.ButtonText = "Beta Features Available";
            this.bunifuFlatButton10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton10, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton10, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton10, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton10.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton10.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton10.ForeColor = System.Drawing.Color.Coral;
            this.bunifuFlatButton10.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton10.Iconimage = null;
            this.bunifuFlatButton10.Iconimage_right = null;
            this.bunifuFlatButton10.Iconimage_right_Selected = null;
            this.bunifuFlatButton10.Iconimage_Selected = null;
            this.bunifuFlatButton10.IconMarginLeft = 0;
            this.bunifuFlatButton10.IconMarginRight = 0;
            this.bunifuFlatButton10.IconRightVisible = true;
            this.bunifuFlatButton10.IconRightZoom = 0D;
            this.bunifuFlatButton10.IconVisible = true;
            this.bunifuFlatButton10.IconZoom = 75D;
            this.bunifuFlatButton10.IsTab = true;
            this.bunifuFlatButton10.Location = new System.Drawing.Point(0, 0);
            this.bunifuFlatButton10.Name = "bunifuFlatButton10";
            this.bunifuFlatButton10.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton10.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(42)))), ((int)(((byte)(46)))));
            this.bunifuFlatButton10.OnHoverTextColor = System.Drawing.Color.Fuchsia;
            this.bunifuFlatButton10.selected = true;
            this.bunifuFlatButton10.Size = new System.Drawing.Size(199, 48);
            this.bunifuFlatButton10.TabIndex = 5;
            this.bunifuFlatButton10.Text = "Beta Features Available";
            this.bunifuFlatButton10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton10.Textcolor = System.Drawing.Color.Violet;
            this.bunifuFlatButton10.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton10.Click += new System.EventHandler(this.BunifuFlatButton10_Click);
            // 
            // bunifuFlatButton11
            // 
            this.bunifuFlatButton11.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton11.BorderRadius = 0;
            this.bunifuFlatButton11.ButtonText = "Auto Updater";
            this.bunifuFlatButton11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton11, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton11, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton11, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton11.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton11.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton11.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton11.Iconimage = null;
            this.bunifuFlatButton11.Iconimage_right = null;
            this.bunifuFlatButton11.Iconimage_right_Selected = null;
            this.bunifuFlatButton11.Iconimage_Selected = null;
            this.bunifuFlatButton11.IconMarginLeft = 0;
            this.bunifuFlatButton11.IconMarginRight = 0;
            this.bunifuFlatButton11.IconRightVisible = true;
            this.bunifuFlatButton11.IconRightZoom = 0D;
            this.bunifuFlatButton11.IconVisible = true;
            this.bunifuFlatButton11.IconZoom = 75D;
            this.bunifuFlatButton11.IsTab = true;
            this.bunifuFlatButton11.Location = new System.Drawing.Point(0, 48);
            this.bunifuFlatButton11.Name = "bunifuFlatButton11";
            this.bunifuFlatButton11.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton11.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(42)))), ((int)(((byte)(46)))));
            this.bunifuFlatButton11.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton11.selected = true;
            this.bunifuFlatButton11.Size = new System.Drawing.Size(199, 48);
            this.bunifuFlatButton11.TabIndex = 6;
            this.bunifuFlatButton11.Text = "Auto Updater";
            this.bunifuFlatButton11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton11.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton11.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton11.Click += new System.EventHandler(this.BunifuFlatButton11_Click_1);
            // 
            // bunifuFlatButton12
            // 
            this.bunifuFlatButton12.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton12.BorderRadius = 0;
            this.bunifuFlatButton12.ButtonText = "Extensions";
            this.bunifuFlatButton12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton12, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton12, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton12, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton12.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton12.Dock = System.Windows.Forms.DockStyle.Top;
            this.bunifuFlatButton12.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton12.Iconimage = null;
            this.bunifuFlatButton12.Iconimage_right = null;
            this.bunifuFlatButton12.Iconimage_right_Selected = null;
            this.bunifuFlatButton12.Iconimage_Selected = null;
            this.bunifuFlatButton12.IconMarginLeft = 0;
            this.bunifuFlatButton12.IconMarginRight = 0;
            this.bunifuFlatButton12.IconRightVisible = true;
            this.bunifuFlatButton12.IconRightZoom = 0D;
            this.bunifuFlatButton12.IconVisible = true;
            this.bunifuFlatButton12.IconZoom = 75D;
            this.bunifuFlatButton12.IsTab = true;
            this.bunifuFlatButton12.Location = new System.Drawing.Point(0, 96);
            this.bunifuFlatButton12.Name = "bunifuFlatButton12";
            this.bunifuFlatButton12.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton12.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(39)))), ((int)(((byte)(42)))), ((int)(((byte)(46)))));
            this.bunifuFlatButton12.OnHoverTextColor = System.Drawing.Color.Silver;
            this.bunifuFlatButton12.selected = true;
            this.bunifuFlatButton12.Size = new System.Drawing.Size(199, 48);
            this.bunifuFlatButton12.TabIndex = 7;
            this.bunifuFlatButton12.Text = "Extensions";
            this.bunifuFlatButton12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton12.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton12.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton12.Click += new System.EventHandler(this.BunifuFlatButton12_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.IMGAni.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label1, BunifuAnimatorNS.DecorationType.None);
            this.label1.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(17, 36);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(714, 40);
            this.label1.TabIndex = 13;
            this.label1.Text = "All Monolith Launcher BETA Features will be listed here, in this panel you can co" +
    "nfigurate, manage and turn\r\nOn or Off features";
            // 
            // bunifuCards1
            // 
            this.bunifuCards1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards1.BorderRadius = 5;
            this.bunifuCards1.BottomSahddow = true;
            this.bunifuCards1.color = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards1.Controls.Add(this.richTextBox1);
            this.bunifuCards1.Controls.Add(this.label3);
            this.bunifuCards1.Controls.Add(this.bunifuSeparator4);
            this.bunifuCards1.Controls.Add(this.label2);
            this.bunifuCards1.Controls.Add(this.label8);
            this.bunifuCards1.Controls.Add(this.fsbs);
            this.AlertAni.SetDecoration(this.bunifuCards1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuCards1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuCards1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards1.LeftSahddow = false;
            this.bunifuCards1.Location = new System.Drawing.Point(22, 105);
            this.bunifuCards1.Name = "bunifuCards1";
            this.bunifuCards1.RightSahddow = true;
            this.bunifuCards1.ShadowDepth = 20;
            this.bunifuCards1.Size = new System.Drawing.Size(393, 246);
            this.bunifuCards1.TabIndex = 14;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.IMGAni.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label2, BunifuAnimatorNS.DecorationType.None);
            this.label2.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label2.Location = new System.Drawing.Point(24, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 20);
            this.label2.TabIndex = 15;
            this.label2.Text = "Enable / Disable Feature :";
            // 
            // bunifuSeparator4
            // 
            this.bunifuSeparator4.BackColor = System.Drawing.Color.Transparent;
            this.MenuAni.SetDecoration(this.bunifuSeparator4, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuSeparator4, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuSeparator4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator4.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSeparator4.LineThickness = 1;
            this.bunifuSeparator4.Location = new System.Drawing.Point(14, 45);
            this.bunifuSeparator4.Name = "bunifuSeparator4";
            this.bunifuSeparator4.Size = new System.Drawing.Size(365, 15);
            this.bunifuSeparator4.TabIndex = 16;
            this.bunifuSeparator4.Transparency = 255;
            this.bunifuSeparator4.Vertical = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.IMGAni.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label3, BunifuAnimatorNS.DecorationType.None);
            this.label3.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label3.Location = new System.Drawing.Point(25, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 20);
            this.label3.TabIndex = 17;
            this.label3.Text = "Information about feature";
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.richTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AlertAni.SetDecoration(this.richTextBox1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.richTextBox1, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.richTextBox1, BunifuAnimatorNS.DecorationType.None);
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.richTextBox1.Location = new System.Drawing.Point(28, 158);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(336, 38);
            this.richTextBox1.TabIndex = 18;
            this.richTextBox1.Text = "Maximize the Monolith Launcher to full-screen mode, get a bigger full-screen expe" +
    "rience of the Monolith Launcher.";
            // 
            // bunifuCards2
            // 
            this.bunifuCards2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards2.BorderRadius = 5;
            this.bunifuCards2.BottomSahddow = true;
            this.bunifuCards2.color = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards2.Controls.Add(this.label16);
            this.bunifuCards2.Controls.Add(this.richTextBox2);
            this.bunifuCards2.Controls.Add(this.label12);
            this.bunifuCards2.Controls.Add(this.bunifuSeparator6);
            this.bunifuCards2.Controls.Add(this.label14);
            this.bunifuCards2.Controls.Add(this.label15);
            this.bunifuCards2.Controls.Add(this.bunifuiOSSwitch1);
            this.AlertAni.SetDecoration(this.bunifuCards2, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuCards2, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuCards2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards2.LeftSahddow = false;
            this.bunifuCards2.Location = new System.Drawing.Point(421, 105);
            this.bunifuCards2.Name = "bunifuCards2";
            this.bunifuCards2.RightSahddow = true;
            this.bunifuCards2.ShadowDepth = 20;
            this.bunifuCards2.Size = new System.Drawing.Size(393, 246);
            this.bunifuCards2.TabIndex = 15;
            // 
            // richTextBox2
            // 
            this.richTextBox2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.richTextBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.AlertAni.SetDecoration(this.richTextBox2, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.richTextBox2, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.richTextBox2, BunifuAnimatorNS.DecorationType.None);
            this.richTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.richTextBox2.Location = new System.Drawing.Point(28, 158);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.ReadOnly = true;
            this.richTextBox2.Size = new System.Drawing.Size(336, 49);
            this.richTextBox2.TabIndex = 18;
            this.richTextBox2.Text = "Change the appearance of the Monolith Launcher, go on the dark-side, or join the " +
    "light-side, which is your preference?";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.IMGAni.SetDecoration(this.label12, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label12, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label12, BunifuAnimatorNS.DecorationType.None);
            this.label12.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label12.Location = new System.Drawing.Point(25, 119);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(181, 20);
            this.label12.TabIndex = 17;
            this.label12.Text = "Information about feature";
            // 
            // bunifuSeparator6
            // 
            this.bunifuSeparator6.BackColor = System.Drawing.Color.Transparent;
            this.MenuAni.SetDecoration(this.bunifuSeparator6, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuSeparator6, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuSeparator6, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator6.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSeparator6.LineThickness = 1;
            this.bunifuSeparator6.Location = new System.Drawing.Point(14, 45);
            this.bunifuSeparator6.Name = "bunifuSeparator6";
            this.bunifuSeparator6.Size = new System.Drawing.Size(365, 15);
            this.bunifuSeparator6.TabIndex = 16;
            this.bunifuSeparator6.Transparency = 255;
            this.bunifuSeparator6.Vertical = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.IMGAni.SetDecoration(this.label14, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label14, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label14, BunifuAnimatorNS.DecorationType.None);
            this.label14.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label14.Location = new System.Drawing.Point(24, 79);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(103, 20);
            this.label14.TabIndex = 15;
            this.label14.Text = "Dark Theme >";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.IMGAni.SetDecoration(this.label15, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label15, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label15, BunifuAnimatorNS.DecorationType.None);
            this.label15.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.White;
            this.label15.Location = new System.Drawing.Point(29, 16);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(316, 21);
            this.label15.TabIndex = 12;
            this.label15.Text = "Monolith Launcher Apperance [ Disabled ]";
            // 
            // bunifuiOSSwitch1
            // 
            this.bunifuiOSSwitch1.BackColor = System.Drawing.Color.Transparent;
            this.bunifuiOSSwitch1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bunifuiOSSwitch1.BackgroundImage")));
            this.bunifuiOSSwitch1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuiOSSwitch1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuiOSSwitch1, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuiOSSwitch1, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuiOSSwitch1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuiOSSwitch1.Location = new System.Drawing.Point(131, 77);
            this.bunifuiOSSwitch1.Name = "bunifuiOSSwitch1";
            this.bunifuiOSSwitch1.OffColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuiOSSwitch1.OnColor = System.Drawing.Color.Silver;
            this.bunifuiOSSwitch1.Size = new System.Drawing.Size(43, 25);
            this.bunifuiOSSwitch1.TabIndex = 3;
            this.bunifuiOSSwitch1.Value = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.IMGAni.SetDecoration(this.label16, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label16, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label16, BunifuAnimatorNS.DecorationType.None);
            this.label16.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label16.Location = new System.Drawing.Point(180, 79);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(105, 20);
            this.label16.TabIndex = 19;
            this.label16.Text = "< Light Theme";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.IMGAni.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label5, BunifuAnimatorNS.DecorationType.None);
            this.label5.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label5.Location = new System.Drawing.Point(17, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(363, 20);
            this.label5.TabIndex = 14;
            this.label5.Text = "Modify the Monolith Launcher Update configurations.";
            // 
            // bunifuCards3
            // 
            this.bunifuCards3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards3.BorderRadius = 5;
            this.bunifuCards3.BottomSahddow = true;
            this.bunifuCards3.color = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards3.Controls.Add(this.label17);
            this.bunifuCards3.Controls.Add(this.bunifuSeparator5);
            this.bunifuCards3.Controls.Add(this.label19);
            this.bunifuCards3.Controls.Add(this.bunifuFlatButton1);
            this.AlertAni.SetDecoration(this.bunifuCards3, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuCards3, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuCards3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards3.LeftSahddow = false;
            this.bunifuCards3.Location = new System.Drawing.Point(22, 96);
            this.bunifuCards3.Name = "bunifuCards3";
            this.bunifuCards3.RightSahddow = true;
            this.bunifuCards3.ShadowDepth = 20;
            this.bunifuCards3.Size = new System.Drawing.Size(393, 190);
            this.bunifuCards3.TabIndex = 15;
            // 
            // bunifuSeparator5
            // 
            this.bunifuSeparator5.BackColor = System.Drawing.Color.Transparent;
            this.MenuAni.SetDecoration(this.bunifuSeparator5, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuSeparator5, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuSeparator5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator5.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSeparator5.LineThickness = 1;
            this.bunifuSeparator5.Location = new System.Drawing.Point(14, 45);
            this.bunifuSeparator5.Name = "bunifuSeparator5";
            this.bunifuSeparator5.Size = new System.Drawing.Size(365, 15);
            this.bunifuSeparator5.TabIndex = 16;
            this.bunifuSeparator5.Transparency = 255;
            this.bunifuSeparator5.Vertical = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.IMGAni.SetDecoration(this.label19, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label19, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label19, BunifuAnimatorNS.DecorationType.None);
            this.label19.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.ForeColor = System.Drawing.Color.White;
            this.label19.Location = new System.Drawing.Point(29, 16);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(210, 21);
            this.label19.TabIndex = 12;
            this.label19.Text = "Check for available updates";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.IMGAni.SetDecoration(this.label17, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label17, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label17, BunifuAnimatorNS.DecorationType.None);
            this.label17.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label17.Location = new System.Drawing.Point(21, 63);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(218, 60);
            this.label17.TabIndex = 17;
            this.label17.Text = "Monolith Launcher Version 1.1.0\r\nUpdate Released 11/22/2019\r\nUpdate by Rondell#59" +
    "07";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.IMGAni.SetDecoration(this.label18, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label18, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label18, BunifuAnimatorNS.DecorationType.None);
            this.label18.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label18.Location = new System.Drawing.Point(18, 36);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(768, 20);
            this.label18.TabIndex = 15;
            this.label18.Text = "These are extensions that the Monolith Launcher uses. (Modifying the app or using" +
    " custom extensions isn\'t allowed)";
            // 
            // bunifuCards4
            // 
            this.bunifuCards4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards4.BorderRadius = 5;
            this.bunifuCards4.BottomSahddow = true;
            this.bunifuCards4.color = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards4.Controls.Add(this.pictureBox3);
            this.bunifuCards4.Controls.Add(this.label20);
            this.bunifuCards4.Controls.Add(this.bunifuSeparator7);
            this.bunifuCards4.Controls.Add(this.label21);
            this.bunifuCards4.Controls.Add(this.bunifuFlatButton3);
            this.AlertAni.SetDecoration(this.bunifuCards4, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuCards4, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuCards4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards4.LeftSahddow = false;
            this.bunifuCards4.Location = new System.Drawing.Point(22, 96);
            this.bunifuCards4.Name = "bunifuCards4";
            this.bunifuCards4.RightSahddow = true;
            this.bunifuCards4.ShadowDepth = 20;
            this.bunifuCards4.Size = new System.Drawing.Size(511, 231);
            this.bunifuCards4.TabIndex = 16;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.IMGAni.SetDecoration(this.label20, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label20, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label20, BunifuAnimatorNS.DecorationType.None);
            this.label20.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label20.Location = new System.Drawing.Point(20, 60);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(485, 100);
            this.label20.TabIndex = 17;
            this.label20.Text = "Extension created by Ravibpatel\r\n\r\nAutoUpdater.NET is a class library that allows" +
    " .NET developers to \r\neasily add auto update functionality to their classic desk" +
    "top application \r\nprojects. ";
            // 
            // bunifuSeparator7
            // 
            this.bunifuSeparator7.BackColor = System.Drawing.Color.Transparent;
            this.MenuAni.SetDecoration(this.bunifuSeparator7, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuSeparator7, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuSeparator7, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator7.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSeparator7.LineThickness = 1;
            this.bunifuSeparator7.Location = new System.Drawing.Point(12, 42);
            this.bunifuSeparator7.Name = "bunifuSeparator7";
            this.bunifuSeparator7.Size = new System.Drawing.Size(487, 15);
            this.bunifuSeparator7.TabIndex = 16;
            this.bunifuSeparator7.Transparency = 255;
            this.bunifuSeparator7.Vertical = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.IMGAni.SetDecoration(this.label21, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label21, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label21, BunifuAnimatorNS.DecorationType.None);
            this.label21.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.ForeColor = System.Drawing.Color.White;
            this.label21.Location = new System.Drawing.Point(29, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(159, 21);
            this.label21.TabIndex = 12;
            this.label21.Text = "AutoUpdater.NET.dll";
            // 
            // bunifuFlatButton3
            // 
            this.bunifuFlatButton3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton3.BorderRadius = 0;
            this.bunifuFlatButton3.ButtonText = "View";
            this.bunifuFlatButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton3.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton3.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton3.Iconimage = null;
            this.bunifuFlatButton3.Iconimage_right = null;
            this.bunifuFlatButton3.Iconimage_right_Selected = null;
            this.bunifuFlatButton3.Iconimage_Selected = null;
            this.bunifuFlatButton3.IconMarginLeft = 0;
            this.bunifuFlatButton3.IconMarginRight = 0;
            this.bunifuFlatButton3.IconRightVisible = true;
            this.bunifuFlatButton3.IconRightZoom = 0D;
            this.bunifuFlatButton3.IconVisible = true;
            this.bunifuFlatButton3.IconZoom = 50D;
            this.bunifuFlatButton3.IsTab = false;
            this.bunifuFlatButton3.Location = new System.Drawing.Point(400, 179);
            this.bunifuFlatButton3.Name = "bunifuFlatButton3";
            this.bunifuFlatButton3.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton3.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton3.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton3.selected = false;
            this.bunifuFlatButton3.Size = new System.Drawing.Size(83, 35);
            this.bunifuFlatButton3.TabIndex = 13;
            this.bunifuFlatButton3.Text = "View";
            this.bunifuFlatButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton3.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton3.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton3.Click += new System.EventHandler(this.BunifuFlatButton3_Click_2);
            // 
            // bunifuCards5
            // 
            this.bunifuCards5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards5.BorderRadius = 5;
            this.bunifuCards5.BottomSahddow = true;
            this.bunifuCards5.color = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuCards5.Controls.Add(this.pictureBox4);
            this.bunifuCards5.Controls.Add(this.label22);
            this.bunifuCards5.Controls.Add(this.bunifuSeparator8);
            this.bunifuCards5.Controls.Add(this.label23);
            this.bunifuCards5.Controls.Add(this.bunifuFlatButton4);
            this.AlertAni.SetDecoration(this.bunifuCards5, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuCards5, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuCards5, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCards5.LeftSahddow = false;
            this.bunifuCards5.Location = new System.Drawing.Point(22, 333);
            this.bunifuCards5.Name = "bunifuCards5";
            this.bunifuCards5.RightSahddow = true;
            this.bunifuCards5.ShadowDepth = 20;
            this.bunifuCards5.Size = new System.Drawing.Size(511, 231);
            this.bunifuCards5.TabIndex = 17;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.IMGAni.SetDecoration(this.label22, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label22, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label22, BunifuAnimatorNS.DecorationType.None);
            this.label22.Font = new System.Drawing.Font("Segoe UI", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.label22.Location = new System.Drawing.Point(20, 60);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(440, 60);
            this.label22.TabIndex = 17;
            this.label22.Text = "Extension created by Bunifu Frameworks\r\n\r\nCarefully crafted controls & components" +
    " for great user experiences";
            // 
            // bunifuSeparator8
            // 
            this.bunifuSeparator8.BackColor = System.Drawing.Color.Transparent;
            this.MenuAni.SetDecoration(this.bunifuSeparator8, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuSeparator8, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.bunifuSeparator8, BunifuAnimatorNS.DecorationType.None);
            this.bunifuSeparator8.LineColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.bunifuSeparator8.LineThickness = 1;
            this.bunifuSeparator8.Location = new System.Drawing.Point(12, 42);
            this.bunifuSeparator8.Name = "bunifuSeparator8";
            this.bunifuSeparator8.Size = new System.Drawing.Size(487, 15);
            this.bunifuSeparator8.TabIndex = 16;
            this.bunifuSeparator8.Transparency = 255;
            this.bunifuSeparator8.Vertical = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.IMGAni.SetDecoration(this.label23, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.label23, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.label23, BunifuAnimatorNS.DecorationType.None);
            this.label23.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.ForeColor = System.Drawing.Color.White;
            this.label23.Location = new System.Drawing.Point(29, 16);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(145, 21);
            this.label23.TabIndex = 12;
            this.label23.Text = "Bunifu_UI_v1.52.dll";
            // 
            // bunifuFlatButton4
            // 
            this.bunifuFlatButton4.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton4.BorderRadius = 0;
            this.bunifuFlatButton4.ButtonText = "View";
            this.bunifuFlatButton4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.IMGAni.SetDecoration(this.bunifuFlatButton4, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.bunifuFlatButton4, BunifuAnimatorNS.DecorationType.None);
            this.MenuAni.SetDecoration(this.bunifuFlatButton4, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton4.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton4.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton4.Iconimage = null;
            this.bunifuFlatButton4.Iconimage_right = null;
            this.bunifuFlatButton4.Iconimage_right_Selected = null;
            this.bunifuFlatButton4.Iconimage_Selected = null;
            this.bunifuFlatButton4.IconMarginLeft = 0;
            this.bunifuFlatButton4.IconMarginRight = 0;
            this.bunifuFlatButton4.IconRightVisible = true;
            this.bunifuFlatButton4.IconRightZoom = 0D;
            this.bunifuFlatButton4.IconVisible = true;
            this.bunifuFlatButton4.IconZoom = 50D;
            this.bunifuFlatButton4.IsTab = false;
            this.bunifuFlatButton4.Location = new System.Drawing.Point(400, 179);
            this.bunifuFlatButton4.Name = "bunifuFlatButton4";
            this.bunifuFlatButton4.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton4.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.bunifuFlatButton4.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton4.selected = false;
            this.bunifuFlatButton4.Size = new System.Drawing.Size(83, 35);
            this.bunifuFlatButton4.TabIndex = 13;
            this.bunifuFlatButton4.Text = "View";
            this.bunifuFlatButton4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton4.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton4.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuFlatButton4.Click += new System.EventHandler(this.BunifuFlatButton4_Click_1);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::Monolith_Launcher_2.Properties.Resources.checkmark_52px;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MenuAni.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.pictureBox3, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox3.Location = new System.Drawing.Point(464, 9);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(35, 28);
            this.pictureBox3.TabIndex = 18;
            this.pictureBox3.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox3, "This is a mandatory extension for the application to function properly.");
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::Monolith_Launcher_2.Properties.Resources.checkmark_52px;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.MenuAni.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this.pictureBox4, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox4.Location = new System.Drawing.Point(464, 9);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(35, 28);
            this.pictureBox4.TabIndex = 19;
            this.pictureBox4.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox4, "This is a mandatory extension for the application to function properly.");
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(47)))), ((int)(((byte)(49)))), ((int)(((byte)(54)))));
            this.ClientSize = new System.Drawing.Size(1308, 748);
            this.Controls.Add(this.SettingsPane);
            this.Controls.Add(this.GModPane);
            this.Controls.Add(this.SideMenu);
            this.Controls.Add(this.ToolbarMain);
            this.MenuAni.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.IMGAni.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.AlertAni.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Monolith Launcher";
            this.ToolbarMain.ResumeLayout(false);
            this.ToolbarMain.PerformLayout();
            this.GModPane.ResumeLayout(false);
            this.mrp_infotab.ResumeLayout(false);
            this.nc1.ResumeLayout(false);
            this.nc1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.SocialP.ResumeLayout(false);
            this.SocialP.PerformLayout();
            this.CommunityP.ResumeLayout(false);
            this.CommunityP.PerformLayout();
            this.MRPP.ResumeLayout(false);
            this.MRPP.PerformLayout();
            this.NewsNInfo.ResumeLayout(false);
            this.NewsNInfo.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.SettingsPane.ResumeLayout(false);
            this.SettingsPane.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.BetaCard1.ResumeLayout(false);
            this.BetaCard1.PerformLayout();
            this.ExtensionCard1.ResumeLayout(false);
            this.ExtensionCard1.PerformLayout();
            this.UpdateCard1.ResumeLayout(false);
            this.UpdateCard1.PerformLayout();
            this.SideMenu.ResumeLayout(false);
            this.SideMenu.PerformLayout();
            this.bunifuCards1.ResumeLayout(false);
            this.bunifuCards1.PerformLayout();
            this.bunifuCards2.ResumeLayout(false);
            this.bunifuCards2.PerformLayout();
            this.bunifuCards3.ResumeLayout(false);
            this.bunifuCards3.PerformLayout();
            this.bunifuCards4.ResumeLayout(false);
            this.bunifuCards4.PerformLayout();
            this.bunifuCards5.ResumeLayout(false);
            this.bunifuCards5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Bunifu.Framework.UI.BunifuElipse RoundEdge_Main;
        private System.Windows.Forms.Panel ToolbarMain;
        private Bunifu.Framework.UI.BunifuDragControl DragControl_Main;
        private System.Windows.Forms.Panel SideMenu;
        private Bunifu.Framework.UI.BunifuFlatButton playbttn;
        private BunifuAnimatorNS.BunifuTransition IMGAni;
        private BunifuAnimatorNS.BunifuTransition MenuAni;
        private System.Windows.Forms.Timer processchk;
        private Bunifu.Framework.UI.BunifuFlatButton MonoInfoBttn;
        private System.Windows.Forms.Panel GModPane;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton5;
        private System.Windows.Forms.Panel SettingsPane;
        private BunifuAnimatorNS.BunifuTransition AlertAni;
        private Bunifu.Framework.UI.BunifuCards UpdateCard1;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label InfoLabel;
        private Bunifu.Framework.UI.BunifuCards ExtensionCard1;
        private System.Windows.Forms.ImageList imageList1;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator2;
        private System.Windows.Forms.Label label7;
        private Bunifu.Framework.UI.BunifuFlatButton xbttn;
        private Bunifu.Framework.UI.BunifuFlatButton minibttn;
        private Bunifu.Framework.UI.BunifuFlatButton maxbttn;
        private Bunifu.Framework.UI.BunifuCards BetaCard1;
        private System.Windows.Forms.Label label8;
        private Bunifu.Framework.UI.BunifuiOSSwitch fsbs;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator3;
        private System.Windows.Forms.Label label6;
        private Bunifu.Framework.UI.BunifuCards nc1;
        private System.Windows.Forms.Label nt1;
        private System.Windows.Forms.RichTextBox nb1;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Timer crashchk;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private System.Windows.Forms.Panel mrp_infotab;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton9;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton8;
        private System.Windows.Forms.Panel panel3;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.WebBrowser axWebBrowser1;
        private System.Windows.Forms.Panel NewsNInfo;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.Panel MRPP;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton23;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton24;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton25;
        private System.Windows.Forms.LinkLabel linkLabel3;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton6;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton19;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton22;
        private System.Windows.Forms.Panel CommunityP;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton26;
        private System.Windows.Forms.LinkLabel linkLabel4;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton29;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton30;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton31;
        private System.Windows.Forms.Panel SocialP;
        private System.Windows.Forms.LinkLabel linkLabel5;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton28;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton33;
        private System.Windows.Forms.Label label11;
        private Bunifu.Framework.UI.BunifuCircleProgressbar bunifuCircleProgressbar1;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton12;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton11;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton10;
        private Bunifu.Framework.UI.BunifuCards bunifuCards5;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label22;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator8;
        private System.Windows.Forms.Label label23;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton4;
        private Bunifu.Framework.UI.BunifuCards bunifuCards4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label20;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator7;
        private System.Windows.Forms.Label label21;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton3;
        private System.Windows.Forms.Label label18;
        private Bunifu.Framework.UI.BunifuCards bunifuCards3;
        private System.Windows.Forms.Label label17;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator5;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label5;
        private Bunifu.Framework.UI.BunifuCards bunifuCards2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private System.Windows.Forms.Label label12;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator6;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private Bunifu.Framework.UI.BunifuiOSSwitch bunifuiOSSwitch1;
        private Bunifu.Framework.UI.BunifuCards bunifuCards1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Label label3;
        private Bunifu.Framework.UI.BunifuSeparator bunifuSeparator4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}

