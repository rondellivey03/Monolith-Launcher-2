﻿using System;
using System.Windows.Forms;
using System.Diagnostics;
using AutoUpdaterDotNET;
using System.Media;

namespace Monolith_Launcher_2
{
    public partial class Form1 : Form
    {
        
        public Form1()
        {
            InitializeComponent();
            GModPane.Dock = DockStyle.Fill;
            SettingsPane.Dock = DockStyle.Fill;
            AutoUpdater.Start("https://raw.githubusercontent.com/rondellivey03/Monolith-Launcher-2/master/Monolith%20Launcher%202/info.xml");
            InfoLabel.Text = ("Version " + Application.ProductVersion + " : Pre-Release");
            if (Properties.Settings.Default.notify == "true")
            {
                nc1.Show();
            }            
        }
        
       
        private void BunifuFlatButton2_Click(object sender, EventArgs e)
        {
            if (GModPane.Visible == true)
            {
                               
                 MenuAni.HideSync(GModPane);
                 MenuAni.ShowSync(SettingsPane);
                 Properties.Settings.Default.Tab = "Settings";
                Properties.Settings.Default.Save();

            }
        }

        private void BunifuFlatButton1_Click(object sender, EventArgs e)
        {            
            Process[] hl2 = Process.GetProcessesByName("hl2");
            if (hl2.Length == 0)
            {
               Process.Start("steam://connect/208.103.169.58:27015");                
            }
            else
            {
                if ((Control.ModifierKeys & Keys.Shift) == Keys.Shift)
                {
                    hl2[0].Kill();
                }
                else
                {
                    MenuAni.ShowSync(nc1);
                    nt1.Text = "Process Active...";
                    nb1.Text = "HL2.exe is already active, Shift + Click play button to kill task.";
                    SystemSounds.Exclamation.Play();
                }
            }
            
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            Process[] hl2 = Process.GetProcessesByName("hl2");
            if (hl2.Length == 0)
            {
                playbttn.Iconimage = Properties.Resources.play_24px;
            }
            else
            {
                playbttn.Iconimage = Properties.Resources.play_52px;
            }
        }

        private void BunifuFlatButton3_Click(object sender, EventArgs e)
        {
            if (SettingsPane.Visible == true)
            {
                                MenuAni.HideSync(SettingsPane);                
                MenuAni.ShowSync(GModPane);
                Properties.Settings.Default.Tab = ("Gmod");
                Properties.Settings.Default.Save();

            }
        }


        private void Label16_Click(object sender, EventArgs e)
        {
            Process.Start("https://f.monolithservers.com/forums/threads/event-poll-minecraft.22500/");
        }      

        private void BunifuFlatButton5_Click(object sender, EventArgs e)
        {
         
          axWebBrowser1.Navigate("https://steamcommunity.com/sharedfiles/filedetails/?id=1861050128");          
        }

        private void BunifuFlatButton6_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void BunifuFlatButton4_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void BunifuFlatButton3_Click_1(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
                maxbttn.Text = "Max";
            }
            else if (this.WindowState == FormWindowState.Normal)
            {
                this.WindowState = FormWindowState.Maximized;
                maxbttn.Text = "?";
            }
        }

        private void BunifuiOSSwitch1_OnValueChange_1(object sender, EventArgs e)
        {
            if (fsbs.Value == true)
            {
                maxbttn.Visible = true;
            }
            else if (fsbs.Value == false)
            {
                maxbttn.Visible = false;
            }
        }

        private void LinkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MenuAni.HideSync(nc1);
            Properties.Settings.Default.notify = "false";
            Properties.Settings.Default.Save();
        }

        private void BunifuFlatButton1_Click_1(object sender, EventArgs e)
        {
            AutoUpdater.Start("https://raw.githubusercontent.com/rondellivey03/Monolith-Launcher-2/master/Monolith%20Launcher%202/info.xml");
            AutoUpdater.ReportErrors = true;
        }   

        private void BunifuFlatButton7_Click(object sender, EventArgs e)
        {           
           axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/announcements.4/");           
        }

        private void BunifuFlatButton8_Click(object sender, EventArgs e)
        {
           axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/rules-information.5/");
        }

        private void BunifuFlatButton9_Click(object sender, EventArgs e)
        {
           axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/updates.43/");
        }

        private void BunifuFlatButton12_Click(object sender, EventArgs e)
        {
           axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/discussion.16/");            
        }

        private void BunifuFlatButton11_Click(object sender, EventArgs e)
        {           
           axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/events.93/");           
        }

        private void LinkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (NewsNInfo.Height == 145)
            {
                NewsNInfo.Height = 35;
                linkLabel2.Text = "► News & Information";
            }
            else if (NewsNInfo.Height == 35)
            {
                NewsNInfo.Height = 145;
                linkLabel2.Text = "▼ News & Information";
            }
        }

        private void LinkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (MRPP.Height == 261)
            {
                MRPP.Height = 35;
                linkLabel3.Text = "► Monolith RP";
            }
            else if (MRPP.Height == 35)
            {
                MRPP.Height = 261;
                linkLabel3.Text = "▼ Monolith RP";
            }
        }

        private void BunifuFlatButton6_Click_1(object sender, EventArgs e)
        {
            axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/discussion.16/");
        }

        private void BunifuFlatButton22_Click(object sender, EventArgs e)
        {
            axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/events.93/");
        }

        private void BunifuFlatButton19_Click(object sender, EventArgs e)
        {
            axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/guides.19/");
        }

        private void BunifuFlatButton23_Click(object sender, EventArgs e)
        {
            axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/roleplay-documents.23/");
        }

        private void BunifuFlatButton25_Click(object sender, EventArgs e)
        {
            axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/refund-requests.25/");
        }

        private void BunifuFlatButton24_Click(object sender, EventArgs e)
        {
            axWebBrowser1.Navigate("https://f.monolithservers.com/forums/forums/feedback.46/");
        }

        private void LinkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (CommunityP.Height == 194)
            {
                CommunityP.Height = 35;
                linkLabel4.Text = "► Community";
            }
            else if (CommunityP.Height == 35)
            {
                CommunityP.Height = 194;
                linkLabel4.Text = "▼ Community";
            }
        }

        private void BunifuFlatButton28_Click(object sender, EventArgs e)
        {
            Process.Start("https://discord.gg/uj6NRBS");
        }

        private void BunifuFlatButton33_Click(object sender, EventArgs e)
        {
            Process.Start("https://steamcommunity.com/groups/MonolithServers");
        }

        private void LinkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            if (SocialP.Height == 125)
            {
                SocialP.Height = 35;
                linkLabel5.Text = "► Social (External Links)";
            }
            else if (SocialP.Height == 35)
            {
                SocialP.Height = 125;
                linkLabel5.Text = "▼ Social (External Links)";
            }
        }

        private void Crashchk_Tick(object sender, EventArgs e)
        {
            if (axWebBrowser1.IsBusy == true)
            {
                bunifuCircleProgressbar1.Visible = true;
            }
            else
            {
                bunifuCircleProgressbar1.Visible = false;
            }            
        }

        private void BunifuFlatButton3_Click_2(object sender, EventArgs e)
        {
            Process.Start("https://github.com/ravibpatel/AutoUpdater.NET");
        }

        private void BunifuFlatButton4_Click_1(object sender, EventArgs e)
        {
            Process.Start("https://bunifuframework.com/products/bunifu-ui-winforms/");
        }

        private void BunifuFlatButton10_Click(object sender, EventArgs e)
        {
            if (UpdateCard1.Visible == true)
            {
                MenuAni.HideSync(UpdateCard1);
                MenuAni.ShowSync(BetaCard1);               

            }
            else if (ExtensionCard1.Visible == true)
            {
                MenuAni.HideSync(ExtensionCard1);
                MenuAni.ShowSync(BetaCard1);
            }
        }

        private void BunifuFlatButton11_Click_1(object sender, EventArgs e)
        {
            if (BetaCard1.Visible == true)
            {
                MenuAni.HideSync(BetaCard1);
                MenuAni.ShowSync(UpdateCard1);
            }
            else if (ExtensionCard1.Visible == true)
            {
                MenuAni.HideSync(ExtensionCard1);
                MenuAni.ShowSync(UpdateCard1);
            }
        }

        private void BunifuFlatButton12_Click_1(object sender, EventArgs e)
        {
            if (BetaCard1.Visible == true)
            {
                MenuAni.HideSync(BetaCard1);
                MenuAni.ShowSync(ExtensionCard1);
            }
            else if (UpdateCard1.Visible == true)
            {
                MenuAni.HideSync(UpdateCard1);
                MenuAni.ShowSync(ExtensionCard1);
            }
        }
    }
}
