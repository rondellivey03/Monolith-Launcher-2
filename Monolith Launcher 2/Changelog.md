# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
## [1.0.11] - 2019.11.15
- Updated UI
-- Updated Sidebar.
-- Removed Sidebar animations.

- Updated components within' the frame to move & resize with-
the window size change, when switching from Minimized to Maximized.
-- Sill tryna figure out that taskbar overlap issue.

- Removed the Sidebar Menu Settings, as it really didn't serve any purpose there.

## [1.0.10] - 2019.11.14
- Beta Features Implemented.
-- Added 'Full Screen Button' into Beta Features.

## [1.0.8] - 2019.11.13
- Improved UI

## [1.0.7] - 2019.11.12
- Removed coin ee.
- Removed unused resources.
- Cleaned up source code.
- Deleted duplicated info.xml
- Fixed manual update check.
- Added missing code to addon button.

## [1.0.6] - 2019.11.11
- Autoupdate Bugfix.
- Implemented Changelog.
- Removed Minecraft Configs (They were unused)
- Bug where you could disable Garry's Mod game, then reload, and then it wouldn't launch, and Garry's Mod wouldn't be on the Launcher. (Strange bug? Fixed though)

## [1.0.5] - 2019.11.08
- Fixed Launch Button, wasn't working.'

## [1.0.4] - 2019.11.08
- Released application officially. No notable changes yet.
