---
name: Bug Report
about: Wanna smash a bug? Yeah, then this is the Template to use!
title: "[BUG] Title Here :)"
labels: bug
assignees: ''

---

### Bug Smash. 

## Bug Information
**Priority:** [I.e: Low, Medium or High]
**What happened?:**
**What did you expect?:**
**When did bug occur?:**

## User Information
**Discord Name:** [I.e. CaptainHook#0001] 
**Did you see [Already Acknowledged Issues?](https://discord.gg/CHUUvmF):**
**What Version are you Using?:**

## Optional Information
- Here, you can submit Error Log Information, Link to Screenshots, or other additional information you believe could be useful to me, much information regarding this issue, the better.
- This is Optional, but if you wish to provide the info, paste it below.
