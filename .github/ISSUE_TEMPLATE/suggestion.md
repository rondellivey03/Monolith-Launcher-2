---
name: Suggestion
about: Suggestions are nice! Submit suggestions here!
title: "[Suggestion] Suggestion Title Here"
labels: enhancement
assignees: ''

---

### Sticky Note: New suggestion request.

## Suggestion Information
**Describe your Suggestion:**
**How does it affect other users?:**
**Got any sources, that might be related to this?:**

## User Information
**Discord Name:** [I.e. CaptainHook#0001] 
**[Did you check Trello?](https://trello.com/b/wxjJQnux/monolith-launcher):**
